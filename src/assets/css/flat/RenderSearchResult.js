import {Platform, StyleSheet} from "react-native";
import {AppColorLightRed} from "../main/Styles";

export default styles = StyleSheet.create({

    MainContianer: {
        borderRadius:4,
        margin:5,
        padding:8,
        flex: 1,
        width: '100%',
        alignSelf:'center',
        backgroundColor: AppColorLightRed,
    },

});
