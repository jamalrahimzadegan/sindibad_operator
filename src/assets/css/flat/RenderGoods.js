import {Platform, StyleSheet} from 'react-native';
import {AppColorLightRed, AppColorRed} from '../main/Styles';

export default styles = StyleSheet.create({
    CatchGiveBtn: {
        marginHorizontal: 15,
        borderRadius: 10,
        paddingHorizontal: 12,
        backgroundColor: AppColorLightRed,
        width: '80%',
        padding: 8,
        height: 40,
        margin: 3,
        ...Platform.select({
            ios: {
                boxWithShadow: {
                    shadowColor: '#000',
                    shadowOffset: {width: 0, height: 1},
                    shadowOpacity: 0.8,
                    shadowRadius: 1,
                },
            },
            android: {
                elevation: 5,
            },
        }),
    },
    DetailsTexts: {
        padding: 5,
        color: '#fff',
        fontSize: 13,
        fontFamily: 'kurdishFont',
    },
});
