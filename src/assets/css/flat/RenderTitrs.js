import {Dimensions, Platform, StyleSheet} from "react-native";
import {AppColorLightRed, AppColorRed} from '../main/Styles';
export default styles = StyleSheet.create({

    MainContianer:{
        flex:1,
        width:'100%',
        backgroundColor:'#fff',

    },
    MarketButton:{
        alignSelf:'center',
        width:'94%',
        borderRadius:3,
        backgroundColor:AppColorLightRed,
        padding:10,
        marginTop:6,
        ...Platform.select({
            ios: {
                boxWithShadow: {
                    shadowColor: '#000',
                    shadowOffset: {width: 0, height: 1},
                    shadowOpacity: 0.8,
                    shadowRadius: 1,
                },
            },
            android: {
                elevation: 5,
            },
        }),
    },
    TitrText:{
        color:'#fff',
        padding:7,
        fontSize:15,
        fontFamily:'kurdishFont',
    }

});
