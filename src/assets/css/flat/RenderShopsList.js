import {Platform, StyleSheet} from 'react-native';
import {AppColorLightRed, AppColorRed} from '../main/Styles';

export default styles = StyleSheet.create({

    RenderListOrderContianer: {
        marginTop: 10,
        flex: 1,
        backgroundColor: '#fff',
        // alignItems: 'center',
    },
    ShopImg: {
        height: 100,
        width: '35%',
        resizeMode: 'contain',
    },
    ListBtnContainer: {
        flex: 1,
        width: '97%',
        alignSelf: 'center',
        padding: 5,
        borderRadius: 5,
        alignItems: 'center',
        overflow: 'hidden',
        flexWrap: 'wrap',
        backgroundColor: AppColorRed,
        margin: 4,
        ...Platform.select({
            ios: {
                boxWithShadow: {
                    shadowColor: '#000',
                    shadowOffset: {width: 0, height: 1},
                    shadowOpacity: 0.8,
                    shadowRadius: 1,
                },
            },
            android: {
                elevation: 10,
            },
        }),
    },
    TextTitle: {
        color: '#fff',
        fontSize: 22,
        fontFamily: 'kurdishFont',
        marginHorizontal: 10,
    },
});
