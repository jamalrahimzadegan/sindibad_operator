import {Platform, StyleSheet} from 'react-native';
import {AppColorLightRed, AppColorRed} from '../main/Styles';

export default styles = StyleSheet.create({

    MainContianer: {
        marginVertical: 20,
        flex: 1,
        width: '96%',
        alignSelf: 'center',
        backgroundColor: '#fff',
    },
    MarketButton: {
        borderRadius: 3,
        backgroundColor: '#ccc',
        padding: 10,
        marginTop: 6,
    },
    map: {
        marginVertical: 15,
        width: '100%',
        height: 200,
    },
    ShopDetails: {
        backgroundColor: '#f0f0f0',
        marginHorizontal: 7,
        padding: 10,
    },
    DetailsTexts: {
        color: '#fff',
        fontSize: 15,
        marginVertical:4,
        fontFamily: 'kurdishFont',
    },
    DetailBtn: {
        backgroundColor: AppColorRed,
        margin: 6,
        borderRadius: 5,
        padding: 11,
        ...Platform.select({
            ios: {
                boxWithShadow: {
                    shadowColor: '#000',
                    shadowOffset: {width: 0, height: 1},
                    shadowOpacity: 0.8,
                    shadowRadius: 1,
                },
            },
            android: {
                elevation: 5,
            },
        }),
    },
    OrderSeperator:{
        width:'100%',
        height:1,
        opacity:.6,
        backgroundColor:'#fff',
        borderRadius:150,
        marginVertical:5
    }
});
