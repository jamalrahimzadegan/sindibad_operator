import {Platform, StyleSheet} from "react-native";
import {AppColorLightRed, AppColorRed} from './Styles';

export default styles = StyleSheet.create({

    DetailsContianer: {
        flex: 1,
        width: '100%',
        backgroundColor: '#fff',
    },
    map: {
        width: '100%',
        height: 200
    },
    TitleDetailsText: {
        color: '#333',
        fontSize: 16,
        padding: 5,
        fontFamily:'kurdishFont',
    },

    ConfirmButton: {
        borderRadius: 2,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 15,
        backgroundColor: AppColorRed,
        height: 50,
        width: '50%'
    },
    SearchBtn: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 3,
        height: 50,
        width: '25%',

    },
    BtnLabel: {
        color: '#fff',
        fontSize: 11,
        fontFamily:'kurdishFont',
        marginTop:5
    },
    ShopBtns:        {
            alignItems: 'center',
            flexDirection: 'row',
            backgroundColor:AppColorRed,
            justifyContent:'space-around',
            paddingVertical:5,
            borderTopRightRadius:15,
            borderTopLeftRadius:15,
        },

});
