import {StyleSheet,Platform, Dimensions} from 'react-native';
import {AppColorRed} from './Styles';

export default styles = StyleSheet.create({
    MainView: {
        width: '100%', height: Dimensions.get('window').height, justifyContent: 'center', alignItems: 'center'
        , backgroundColor: '#ffffff',
    },
    MainView3: {
        width: '100%',
        height: Dimensions.get('window').height,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
    },
    MainView1: {
        paddingHorizontal:5,
        width: '70%',
        height: 43,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: 12,
        borderRadius: 5,
        overflow: 'hidden',
        flexDirection: 'row',
        ...Platform.select({
            android:{
                elevation:3
            }
        })
    },
    MainView2: {
        paddingHorizontal:5,
        width: '70%',
        height: 43,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: 12,
        borderRadius: 5,
        overflow: 'hidden',
        flexDirection: 'row',
        ...Platform.select({
            android:{
                elevation:3
            }
        })
    },
    loginBtn: {
        width: 220,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        backgroundColor: AppColorRed,
        marginTop: 30,
    },
    btntextd: {color: '#ffffff', fontFamily: 'BYekan', fontSize: 16},
    stlbtnx: {
        width: '70%',
        height: 43,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        backgroundColor: AppColorRed,
        marginTop: 20,
    },
    btntext: {fontFamily: 'BYekan', fontSize: 15, color: '#ffffff'},
    inp: {
        fontFamily: 'BYekan',
        width: '90%',
        height: '100%',
        color: '#808080',
        paddingLeft: 10,
    },
    inp2: {
        width: '85%',
        height: '100%',
        color: '#808080',
        paddingLeft: 10,
        textAlign: 'right',
        alignItems: 'flex-end',
    },
    RulesTitr: {
        fontFamily: 'BYekan',
        color: AppColorRed,
        fontSize: 17,
        padding: 10,
        textAlign: 'center',
    },
    WebViewRules: {
        height: '90%',
        width: '100%',
        backgroundColor: 'transparent',
        position: 'absolute',
        right: 0,
    },
    ConfirmButton: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        backgroundColor: '#bbb',
        borderRadius: 4,
        margin: 10,
        width: '50%',
        height: 50,
        alignSelf: 'center',
    },
    Rules: {
        backgroundColor: AppColorRed,
        alignSelf: 'center',
        alignItems: 'center',
        position: 'absolute',
        width: '90%',
        height: '85%',
        borderRadius: 4,
        padding: 10,
    },
    AcceptRulesText: {
        fontFamily: 'BYekan',
        color: '#333',
        fontSize: 14,
    },
    cityproBtnText: {
        textAlign: 'right',
        color: '#808080',
        paddingRight: 10,
    },
    PorCityBTN: {
        paddingHorizontal:5,
        width: '100%',
        height: '100%',
        color: '#808080',
        textAlign: 'right',
        alignItems: 'center',
        justifyContent: 'flex-end',
        flexDirection: 'row',
    },
});
