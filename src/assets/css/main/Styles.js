import {StyleSheet} from 'react-native';
export const AppColorRed = '#BF0540';
export const AppColorLightRed = '#f2054f';
export const PickerBg = [255, 118, 117, 1];
export const PickerToolBarBg = [214, 48, 49, 1];
export const PickerTitleColor = [255, 255, 255, 1];
export const PickerConfirmBtnColor = [255, 255, 255, 1];
export const PickerCancelBtnColor = [255, 255, 255, 1];
export const PickerFontColor = [0, 0, 0, 1];
export default Styles = StyleSheet.create({
    DropdownStyle: {
        color: '#fff',
        fontSize: 16,
    },
    DropDownContianer:{
        borderBottomLeftRadius:20,
        borderBottomRightRadius:20,
        width:'95%',
        alignSelf:'center',
        backgroundColor:'gold',

    },
    TitleDropDown: {
        color: '#fff',
        fontFamily: 'kurdishFont',
        fontSize: 15

    },
    BodyDropDown: {
        color: '#fff',
        fontFamily: 'kurdishFont',
        fontSize: 13

    },
});


