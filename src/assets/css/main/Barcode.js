
import {StyleSheet} from 'react-native';
import {AppColorRed} from './Styles';
export default styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black',
        // transform: [{rotate: '-90deg'}]

    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
    },
    BarcodeText: {
        backgroundColor: AppColorRed,
        color: '#fff',
        padding: 8,
        borderRadius: 10,
        position: 'absolute',
        bottom: 5,
        left: 4
    },
    SearchBtn: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 150,
        backgroundColor: AppColorRed,
        height: 50,
        width: 50,
        position: 'absolute',
        bottom: 10,
        right: 10
    }
});