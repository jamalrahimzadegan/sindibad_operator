import {StyleSheet, Dimensions, Platform} from 'react-native';
import {AppColorLightRed, AppColorRed} from './Styles';


export default styles = StyleSheet.create({
    MainView: {

        flex: 1,
        width: '100%',
        height: Dimensions.get('window').height,
        backgroundColor: '#fff',

    },
    Container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '96%',
        alignSelf: 'center',
        overflow: 'hidden',
        borderRadius: 3,
    },
    ProfilePhoto: {
        borderRadius:4,
        height: '96%',
        width: '96%',
        resizeMode: 'contain',
    },
    PhotoUploadLoading: {
        position: 'absolute',
        // bottom:0,

    },
    NameText: {
        fontFamily: 'kurdishFont',
        color: AppColorRed,
        fontSize: 25,
        paddingRight: 10,
        width: '100%',
        flexShrink: 1,
    },
    EachLine: {
        marginVertical: 10,
        width: '96%',
        alignSelf: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    seprator: {

    },
    Label: {
        flexShrink: 1,
        fontFamily: 'kurdishFont',
        color: '#333',
        fontSize: 15,
    },
    Input: {
        fontFamily: 'kurdishFont',
        fontSize: 16,
        borderRadius: 4,
        // borderWidth: 1.2,
        color:'#fff',
        width: '65%',
        paddingHorizontal: 10,
        backgroundColor: AppColorRed,
        ...Platform.select({
            ios: {
                boxWithShadow: {
                    shadowColor: '#000',
                    shadowOffset: {width: 0, height: 1},
                    shadowOpacity: 0.8,
                    shadowRadius: 1,
                },
            },
            android: {
                elevation: 5,
            },
        }),
    },
    UpdateButton: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: AppColorRed,
        borderRadius: 5,
        width: '50%',
        height: 45,
        alignSelf: 'center',
        padding: 7,
        marginVertical: 20,
    },
    map: {
        width: '96%',
        alignSelf: 'center',
        height: 250,
    },

});
