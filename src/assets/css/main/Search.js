import {Dimensions, Platform, StyleSheet} from 'react-native';
import {AppColorRed} from './Styles';


export default styles = StyleSheet.create({
    MainContianer: {
        flex: 1,
        width: '100%',
        backgroundColor: '#fff',
    },
    SearchBar: {
        backgroundColor:AppColorRed,
        borderRadius: 5,
        marginVertical: 10,
        padding: 3,
        width: '96%',
        alignSelf: 'center',
        height: 55,
        alignItems: 'center',
        justifyContent: 'space-around',
        flexDirection: 'row',
        ...Platform.select({
            ios: {
                boxWithShadow: {
                    shadowColor: '#000',
                    shadowOffset: {width: 0, height: 1},
                    shadowOpacity: 0.8,
                    shadowRadius: 1,
                },
            },
            android: {
                elevation: 5,
            },
        }),
    },
    InputSearch: {
        width: '85%',
        height: '100%',
        paddingHorizontal: 5,
        fontFamily: 'kurdishFont',
        color:'#fff',

    },
    SearchBtn: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 45,
        width: 45,
    },
});
