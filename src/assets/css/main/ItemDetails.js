import {Dimensions, Platform, StyleSheet} from 'react-native';
import {AppColorRed} from './Styles';


export default styles = StyleSheet.create({

    MainContianer: {
        flex: 1,
        width: '100%',
        backgroundColor: '#fff',

    }, ItemImgCon: {
        overflow: 'hidden',
        backgroundColor: AppColorRed,
    },
    ShopPhoto: {
        width: '100%',
        height: 300,
        resizeMode: 'contain',
        borderTopLeftRadius: 20, borderTopRightRadius: 20,

    },

    DetalsText: {
        textAlign: 'center',
        color: '#333',
        fontSize: 16,
        fontFamily: 'kurdishFont', marginVertical: 20,
    },
    Input: {
        textAlign: 'center',
        width: '100%',
        alignSelf: 'center',
        height: 50,
        borderRadius: 10,
        backgroundColor: '#ddd',
        marginBottom: 20,
        fontFamily: 'kurdishFont',
    },
    AddRmovBtn: {
        // marginBottom:70,
        marginVertical: 50,
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        justifyContent: 'space-between',
    },
    ModalContainer: {
        zIndex: 200,
        flex: 1,
        top: 0,
        bottom: 0,
        right: 0,
        position: 'absolute',
    },
    ModalBase: {
        backgroundColor: 'rgba(46, 49, 49, .6)',
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
    },
    SearchBtn: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 150,
        backgroundColor: AppColorRed,
        height: 50,
        width: 50,
        position: 'absolute',
        bottom: 10,
        right: 10,
    },
});
