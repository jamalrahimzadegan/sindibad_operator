import {StyleSheet, Dimensions, AsyncStorage, Platform} from 'react-native';
import {AppColorLightRed, AppColorRed} from "./Styles";


export default styles = StyleSheet.create({
    MainView: {
        flex: 1,
        width: "100%",
        height: Dimensions.get("window").height,
        backgroundColor: "#ffffff"
    },

    Container: {
        width:'100%',
        padding: 6,
        borderRadius:5,
        marginVertical:5,
        backgroundColor:AppColorRed,
        alignSelf: 'center',
        ...Platform.select({
            ios: {
                boxWithShadow: {
                    shadowColor: '#000',
                    shadowOffset: {width: 0, height: 1},
                    shadowOpacity: 0.8,
                    shadowRadius: 1,
                },
            },
            android: {
                elevation: 5,
            },
        }),
    },
    eachFactorText: {
        color: '#fff',
        fontFamily:'kurdishFont',
        fontSize: 14,
        paddingVertical: 3
    },
    DetailBTN: {
        alignSelf: 'center',
        width: '96%',
        margin: 10,
        borderRadius: 3,
        backgroundColor: AppColorLightRed,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10
    }
});
