import {Dimensions, Platform, StyleSheet} from "react-native";
import {AppColorRed} from "./Styles";
const H = Dimensions.get("window").height;
const W = Dimensions.get("window").width;
export default styles = StyleSheet.create({

    MainContianer:{
        flex:1,
        width:'100%',
        backgroundColor:'#fff',

    },
    MainBtnContainer:{
        alignItems: 'center',
        flexDirection:'row',
        justifyContent: 'space-between'
    },
    MainPageButtons:{
        backgroundColor:AppColorRed,
        alignSelf:'center',
        borderRadius:2,
        padding:7,
        margin:4,
        width: '48%',
        height:55,
        justifyContent:'center',
        alignItems:'center',
    },
    MainTextButton:{
        color:'#fff',
        fontSize:16,
        fontFamily:'kurdishFont',    },

});
