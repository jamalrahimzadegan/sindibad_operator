import {Dimensions, Platform, StyleSheet} from "react-native";
import {AppColorRed} from "./Styles";
const H = Dimensions.get("window").height;
const W = Dimensions.get("window").width;
export default styles = StyleSheet.create({

    MainContianer:{
        flex:1,
        width:'100%',
        backgroundColor:'#fff',

    },
    SliderImg:{
        height: 200,
        width: W,
        resizeMode: 'cover',
        backgroundColor: '#ccc',
        borderTopLeftRadius: 45 / 2,
        borderTopRightRadius: 45 / 2,
    }
    ,
    MainBtnContainer:{
        alignItems: 'center',
        flexDirection:'row',
        justifyContent: 'space-between'
    },
    MainPageButtons:{
        backgroundColor:AppColorRed,
        alignSelf:'center',
        borderRadius:2,
        padding:7,
        margin:4,
        width: '31%',
        height:50,
        justifyContent:'center',
        alignItems:'center',
    },
    MainTextButton:{
        color:'#fff',
        fontSize:16,
        fontFamily:'kurdishFont',
    },

});
