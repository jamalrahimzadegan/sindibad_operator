import styles from './../assets/css/Header';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon1 from 'react-native-vector-icons/AntDesign';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import React, {Component} from 'react';
import {
    View,
    TouchableOpacity,
    AsyncStorage, StatusBar,
} from 'react-native';
import {NavigationActions, StackActions} from 'react-navigation';
import {AppColorRed} from '../assets/css/main/Styles';
import ItemDetails from './main/ItemDetails';


export default class Header extends Component {
    constructor(props) {
        super(props);
        this.Language = '';
    }

    componentWillMount() {
        AsyncStorage.getItem('language').then((x) => {
            this.Language = x;
            this.forceUpdate();
        });
    }

    render() {
        let Route = this.props.navigation.state.routeName;
        // console.warn('Route: '+Route);
        return (
                <View style={{backgroundColor: AppColorRed}}>
                    <View style={styles.iosStatus}/>
                    <StatusBar backgroundColor={AppColorRed} barStyle="light-content"/>
                    <View
                        style={[styles.NavigationContainer, {flexDirection: this.Language == 'En' ? 'row' : 'row-reverse'}]}>
                        <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                            <Icon name={'md-menu'} color={'#fff'} size={40}/>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.EachButton}
                                          onPress={() => this._GoHome()}>
                            <Icon3 name={'home-circle'} color={'#fff'} size={40}/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Icon1 name={'back'} color={'#fff'} size={32}/>
                        </TouchableOpacity>
                    </View>
                    {
                        Route === 'Main' ||Route=== 'ItemDetails' ? null : <View style={styles.bottomNav}/>
                    }
                </View>
        );
    }

//-------Navigatie to Home Screen (Main)---------------------------------------------------------------------------
    _GoHome() {
        const resetAction = StackActions.reset({
            index: 0,
            key: null,
            actions: [
                NavigationActions.navigate({routeName: 'Main', params: {resetOrder: 1}}),
            ],
        });
        this.props.navigation.dispatch(resetAction);
    };
}













