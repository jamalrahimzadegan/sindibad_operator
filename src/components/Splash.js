﻿import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Linking,
    AsyncStorage,
    Platform,
    Easing,
    Animated, StatusBar,
    ImageBackground,
    Dimensions,
} from 'react-native';
import styles from '../assets/css/main/Splash';
import {Connect} from '../core/Connect';
import URLS from '../core/URLS';
import firebase from 'react-native-firebase';
import Dic from '../core/Dic';
import {connect} from 'react-redux';


class Splash extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Update: 'none',
            indicatorr: 'flex',
            FcmToken: '',
            showLang: 'none',
            rotate: new Animated.Value(0),
        };
        this.Language = '';
    }

    //------------componentWillMount-------------------------------------------------------------------------------------------------------
    componentWillMount() {
        AsyncStorage.multiGet([
            'language',
            'SentTopic',
        ]).then((x) => {
            if (x[1][1] === '1') {
            } else {
                firebase.messaging().subscribeToTopic('all');
                AsyncStorage.setItem('SentTopic', '1');
            }
            this.Language = x[0][1];
            this.forceUpdate();
        });
        this._Animate();
        this._VersionCheck();
    }


    //-- animation--------------------------------------------------------
    spinValue = new Animated.Value(0);

    _Animate() {
        Animated.loop(
            Animated.sequence([
                Animated.timing(
                    this.spinValue,
                    {
                        toValue: 1,
                        duration: 1600,
                        easing: Easing.linear,
                        useNativeDriver: true,
                    },
                ), Animated.timing(
                    this.spinValue,
                    {
                        toValue: 0,
                        duration: 1600,
                        easing: Easing.linear,
                        useNativeDriver: true,
                    },
                ),
            ])).start();
        {
            this.props.navigation.state.routeName !== 'Splash'
                ?
                this.spinValue.stopAnimation() : null;
        }

    }


    //------------render-------------------------------------------------------------------------------------------------------
    render() {
        let Dict = Dic[this.Language === null || this.Language == '' ? 'Ku' : this.Language];
        let spin = this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['-45deg', '45deg'],
        });
        return (
            <ImageBackground style={styles.mainsp}
                             source={require('./../assets/images/SplashBack.jpg')}
                             blurRadius={7}>
                <StatusBar barStyle={'dark-content'} hidden={true}/>

                <View style={{width: '100%', flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                    <Animated.Image source={require('../assets/images/logo.png')}
                                    style={[styles.imgf, {
                                        transform: [{rotateZ: spin}],
                                    }]}/>
                    <View style={{marginTop: 100, width: '100%', height: 40, display: this.state.indicatorr}}>
                    </View>
                    <View style={[styles.UpdateOuter, {display: this.state.Update}]}>
                        <Text style={styles.SplashUpText}>{Dict['new_version']}</Text>
                        <TouchableOpacity
                            style={[styles.SplashUpdateBtn, {
                                display: Platform.select({
                                    android: 'flex',
                                    ios: 'none',
                                }),
                            }]}
                            onPress={() => {
                                Linking.openURL(Platform.select({
                                    ios: 'e',
                                    android: 'https://play.google.com/store/apps/details?id=com.operatorsindibad',
                                }));
                            }}>
                            <Text style={styles.SplashUpdateTextBtn}>{Dict['new_version_download']}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.languagesView, {display: this.state.showLang}]}>
                        <TouchableOpacity style={styles.langbtns} onPress={() => this._setLang('En')}>
                            <Text style={styles.langtext}>English</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.langbtns} onPress={() => this._setLang('Ar')}>
                            <Text style={styles.langtext}>Arabic</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.langbtns} onPress={() => this._setLang('Ku')}>
                            <Text style={styles.langtext}>Kurdish</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.langbtns} onPress={() => this._setLang('Fa')}>
                            <Text style={styles.langtext}>Persian</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ImageBackground>
        );
    }

    //------------_setLang------------------------------------------------------------------------------------------------------------------------------------------
    _setLang(lang) {
        lang === 'En' ? AsyncStorage.multiSet([['align', 'left'], ['language', lang], ['direction', 'row']]) : AsyncStorage.multiSet([['align', 'right'], ['language', lang], ['direction', 'row-reverse']]);
        this.setState({showLang: 'none', indicatorr: 'flex'});
        this.props.drawer_updateFn();
        this._VersionCheck();
    }

    //------------------Checking Version -------------------------------------------------------------------------------
    _VersionCheck() {
        AsyncStorage.getItem('language').then((lang) => {
            if (lang) {
                Connect.SendPRequest(URLS.Link() + 'versionope', {version: '1.0.5'}) // pre was 1.0.4
                    .then(versionResp => {
                        //-----------check in last version-----------------------------------------------------------------------------------
                        if (versionResp.result) {
                            AsyncStorage.setItem('support_phone', versionResp.phone);
                            if (this.props.id) {     //singed UP and logged in
                                this.props.navigation.replace('Main');
                            } else {
                                this.props.navigation.replace('Login');
                            }
                        } else {
                            this.setState({Update: 'flex', indicatorr: 'none'});
                        }
                    });
            } else {
                this.setState({showLang: 'flex', indicatorr: 'none'});
            }
        });
    }
}

//-----------------------Redux--------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,

    };
}

function mapDispatchToProps(dispatch) {
    return {
        drawer_updateFn: () => dispatch({type: 'drawer_updateFn'}),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Splash);