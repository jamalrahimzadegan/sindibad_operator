import {Component} from 'react';
import {
    AsyncStorage,
    Dimensions,
    View,
    Text,
    TouchableOpacity,
    Button,
    TextInput,
    ScrollView,
} from 'react-native';
import React from 'react';
import Styles, {AppColorRed} from '../../assets/css/main/Styles';
import styles from '../../assets/css/main/ItemDetails';
import Header from '../Header';
import URLS from '../../core/URLS';
import FastImage from 'react-native-fast-image';
import {Connect} from '../../core/Connect';
import DropdownAlert from 'react-native-dropdownalert';
import Icon from 'react-native-vector-icons/AntDesign';
import {connect} from 'react-redux';
import Dic from '../../core/Dic';


class ItemDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Empty: false,
            Discount: '',
            Price: '',
            ShowModal: false,
            Count: '',
            Desc: '',
            ItemAction: '',
            barcode: '',
            Details: [],
        };
        this.Language = '';
    }

    componentWillMount() {
        AsyncStorage.getItem('language').then((language) => {
            this.Language = language;
            this.forceUpdate();
        });
        this.props.navigation.addListener('willFocus', payload => {
            let item = this.props.navigation.getParam('item');
            this._GetDetails(item.id, item.shopId);
        });

    }

    render() {
        let {Details} = this.state;
        let item = this.props.navigation.getParam('item');
        let Dict = Dic[this.Language === null || this.Language == '' ? 'Ku' : this.Language];
        return (
            <View style={[styles.MainContianer]}>
                <Header navigation={this.props.navigation}/>
                <ScrollView contentContainerStyle={{}}
                            showsVerticalScrollIndicator={false}
                            style={{flex: 1}}>
                    <View style={styles.ItemImgCon}>
                        {
                            item.image ?
                                <FastImage
                                    source={{uri: item.image ? URLS.MediaThumbnail() + item.image : null}}
                                    style={[styles.ShopPhoto]}
                                /> :
                                <FastImage
                                    source={require('./../../assets/images/online-store.png')}
                                    style={[styles.ShopPhoto]}
                                />
                        }
                    </View>
                    {/*----------Item------------------------------------------------------------------------------------*/}
                    <View style={{width: '100%', paddingVertical: 20, paddingHorizontal: 35}}>
                        <Text
                            style={styles.DetalsText}>{this.Language === 'En' ? item.titleEn : this.Language === 'Fa' ? item.title : this.Language === 'Ar' ? item.titleAr : this.Language === 'Ku' ? item.titleKu : '-'}</Text>
                        <TextInput
                            value={this.state.Discount}
                            onChangeText={(Discount) => this.setState({Discount})}
                            underlineColorAndroid={'transparent'}
                            keyboardType={'number-pad'}
                            returnKeyType={'done'}
                            maxLength={3}
                            placeholder={this.state.Discount ? this.state.Discount : Dict['discount_percent']}
                            style={[styles.Input, {}]}
                        />
                        <TextInput
                            value={this.state.Price}
                            onChangeText={(Price) => this.setState({Price})}
                            underlineColorAndroid={'transparent'}
                            keyboardType={'number-pad'}
                            returnKeyType={'done'}
                            placeholder={this.state.Price ? this.state.Price : Dict['price']}
                            style={[styles.Input, {}]}
                        />
                        <Button onPress={() => this._UpdateItem(item.id, Dict)} color={'#636e72'}
                                title={Dict['t_main_confirm']}/>
                        {/*-----------AddRmovBtns------------------------------------------------------------------------------------*/}
                        <View style={[styles.AddRmovBtn, {display: Details.store == null ? 'none' : 'flex'}]}>
                            <Button onPress={() => {
                                this.setState({
                                    ShowModal: !this.state.ShowModal,
                                    ItemAction: 'add',
                                });
                            }}
                                    color={'#1e824c'} title={Dict['addtostore']}/>
                            <Button onPress={() => {
                                this.setState({
                                    ShowModal: !this.state.ShowModal,
                                    ItemAction: 'remove',
                                });
                            }} color={AppColorRed} title={Dict['remfromstroe']}/>
                        </View>
                    </View>
                </ScrollView>
                {/*-----------Modal----------------------------------------------------------------------------------------*/}
                <View style={[styles.ModalContainer, {left: this.state.ShowModal ? 0 : 2000}]}>
                    <View style={styles.ModalBase}>
                        <View style={{
                            backgroundColor: '#fff',
                            width: '85%',
                            padding: 15,
                            alignItems: 'center',
                            borderRadius: 4,
                        }}>
                            <TextInput
                                value={this.state.Count}
                                onChangeText={(Count) => this.setState({Count})}
                                underlineColorAndroid={'transparent'}
                                keyboardType={'number-pad'}
                                returnKeyType={'done'}
                                placeholder={this.state.Count ? this.state.Count : Dict['count']}
                                style={[styles.Input, {}]}
                            />
                            <TextInput
                                value={this.state.Desc}
                                onChangeText={(Desc) => this.setState({Desc})}
                                underlineColorAndroid={'transparent'}
                                placeholder={this.state.Desc ? this.state.Desc : Dict['description']}
                                multiline={true}
                                style={[styles.Input, {}]}
                            />
                            <View style={[styles.AddRmovBtn, {justifyContent: 'space-around', marginVertical: 15}]}>
                                <Button
                                    onPress={() => this.setState({ShowModal: !this.state.ShowModal})}
                                    color={'#013243'} title={Dict['closing']}/>
                                <Button
                                    onPress={() => {
                                        this._AddRemovItem(Details.store.id, Dict);
                                    }}
                                    color={this.state.ItemAction === 'remove' ? '#cf000f' : '#1e824c'}
                                    title={this.state.ItemAction === 'remove' ? Dict['decrease'] : Dict['increase']}/>
                            </View>
                        </View>
                    </View>
                </View>
                {/*-----------BarcodeBTN----------------------------------------------------------------------------------------*/}
                <TouchableOpacity style={styles.SearchBtn}
                                  onPress={() => this.props.navigation.navigate('Barcode', {
                                      RoutIs: 'ItemDetails',
                                      _ReadBarcode: (barcode) => this.setState({barcode}),
                                  })}>
                    <Icon name={'barcode'} color={'#fff'} size={25} style={{}}/>
                </TouchableOpacity>
                <DropdownAlert ref={ref => this.dropdown = ref}
                               closeInterval={5000}

                               titleStyle={Styles.TitleDropDown}
                               messageStyle={Styles.BodyDropDown}/>
            </View>
        );
    }

    //---------------------UpdateItem-------------------------------------------------------------------------------------
    _UpdateItem(id, Dict) {
        // console.warn('Price: '+this.state.Price)
        // console.warn('Discount: '+this.state.Discount)
        // console.warn('Discount: '+this.state.Discount)
        // console.warn('shopId: '+this.props.navigation.getParam('shopId'))
        this.setState({Empty: true});
        let {Price, Discount} = this.state;
        if (Discount == '' || Price == '' || parseInt(Discount) > 100) {
            // this.dropdown.alertWithType('error', '', 'موارد را به درستی وارد کنید');
        } else {
            Connect.SendPRequest(URLS.Link() + 'setproductstore', {
                shopId: parseInt(this.props.navigation.getParam('shopId')),
                productId: parseInt(id),
                price: parseInt(parseInt(this.state.Price)),
                discount: parseInt(parseInt(this.state.Discount)),
            }).then(res => {
                // console.warn('SetProductStore:' + res);
                // console.warn(res)
                if (res === true) {
                    this.setState({Empty: false});
                    this.dropdown.alertWithType('success', '', Dict['good_finished']);
                } else {
                    this.setState({Empty: false});
                    this.dropdown.alertWithType('error', '', Dict['server_error']);
                }
            });
        }
    }

    //---------------------UpdateItem-------------------------------------------------------------------------------------
    _GetDetails(Itemid, shopID) {
        Connect.SendPRequest(URLS.Link() + 'getproductstore', {
            id: parseInt(Itemid),
            shopId: parseInt(shopID),
        }).then(res => {
            // console.log('getproductstore:');
            // console.log(res);
            if (res) {
                this.setState({Empty: false, Details: res}, () => null);
            } else {
                this.setState({Empty: false});
            }
        });
    }

    //---------------------_AddRemovItem-------------------------------------------------------------------------------------
    _AddRemovItem(id, Dict) {
        let {ItemAction, ShowModal, Count, barcode, Desc} = this.state;
        //     '--Count: ' + Count,
        //     '--Desc: ' + Desc);
        this.setState({Loading: true, ShowModal: !ShowModal});
        if (Count !== '' || Desc !== '') {
            if (ItemAction === 'add') {
                // console.warn('action add')
                Connect.SendPRequest(URLS.Link() + 'addtostore', {
                    // id: 28,
                    id: parseInt(id),
                    userId: parseInt(this.props.id),
                    // barCode: '1635465',
                    barCode: barcode,
                    count: parseInt(this.state.Count),
                    desc: this.state.Desc,
                }).then(res => {
                    // console.warn('addtostore: ' + res);
                    if (res === true) {
                        this.dropdown.alertWithType('success', '', Dict['good_finished']);
                    } else {
                        this.dropdown.alertWithType('error', '', Dict['server_error']);

                    }
                    this.setState({Loading: false, ShowModal: !ShowModal});
                }).catch((e) => {
                    this.setState({Loading: false, ShowModal: !ShowModal});
                    this.dropdown.alertWithType('error', '', Dict['global_error']);
                });
            } else if (ItemAction == 'remove') {
                // console.warn('action remove')
                Connect.SendPRequest(URLS.Link() + 'subfromstore', {
                    // id: 28,
                    id: parseInt(id),
                    userId: parseInt(this.props.id),
                    // barCode: '1635465',
                    barCode: barcode,
                    count: parseInt(this.state.Count),
                    desc: this.state.Desc,
                }).then(res => {
                    // console.warn('subfromstore: ' + res);
                    if (res === true) {
                        this.dropdown.alertWithType('success', '', Dict['good_finished']);
                    } else {
                        this.dropdown.alertWithType('error', '', Dict['server_error']);

                    }
                    this.setState({Loading: false, ShowModal: !ShowModal});
                }).catch((e) => {
                    this.setState({Loading: false, ShowModal: !ShowModal});
                    this.dropdown.alertWithType('error', '', Dict['global_error']);
                });
            }
        } else {
            // this.dropdown.alertWithType('error', '', 'موارد را به درستی وارد کنید');
        }
    }
}

//---------------------redux-------------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
    };
}

function mapDispatchToProps(dispatch) {
    // console.warn('dispatch: ',dispatch)
    return {
        getId: () => dispatch({type: 'getId'}),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemDetails);