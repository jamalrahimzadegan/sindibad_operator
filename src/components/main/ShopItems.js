import {Component} from 'react';
import {AsyncStorage, ActivityIndicator, View, Image, Text, ScrollView, TouchableOpacity, FlatList} from 'react-native';
import React from 'react';
import DropdownAlert from 'react-native-dropdownalert';
import Styles, {AppColorRed} from '../../assets/css/main/Styles';
import styles from '../../assets/css/main/Details';
import Header from '../Header';
import ListEmpty from "../ListEmpty";
import {Connect} from "../../core/Connect";
import URLS from "../../core/URLS";
import RenderTitrs from "../flatlists/RenderTitrs";
import Icon1 from "react-native-vector-icons/AntDesign";
import Icon from "react-native-vector-icons/FontAwesome5";
import Icon3 from "react-native-vector-icons/MaterialCommunityIcons";
import Dic from '../../core/Dic';

export default class ShopItems extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Loading: false,
            Empty: false,
            Titrs: [],
        };
        this.Language = '';
        this._GetCat = this._GetCat.bind(this)
    }

    componentWillMount() {
        this._GetTitles(this.props.navigation.getParam('shopId'));
        AsyncStorage.multiGet([
            'language',
        ]).then((x) => {
            this.Language = x[0][1];
            this.forceUpdate();
        });
    }

    render() {
        let Dict = Dic[this.Language === null|| this.Language ==''? 'Ar' : this.Language];
        return (
            <View style={styles.DetailsContianer}>
                <Header navigation={this.props.navigation}/>
                <View style={{flex:1, marginBottom:5}}>
                    {this.state.Titrs.length > 0 ?
                        <FlatList
                            renderItem={(item, index) => <RenderTitrs
                                shopId={this.props.navigation.getParam('shopId')}
                                item={item}
                                navigation={this.props.navigation}
                            />}
                            showsVerticalScrollIndicator={false}
                            data={this.state.Titrs}
                            keyExtractor={(index) => index.toString()}
                            ListEmptyComponent={() => <ListEmpty
                                EmptyText={this.state.Empty ? Dict['loading'] : Dict['noitem']}
                                BgColor={'transparent'}/>}/>
                        : null
                    }
                </View>
                {/*------Bottom Buttons--------------------------------------------------------------------------------*/}
                <View style={styles.ShopBtns}>
                    {/*------Factors--------------------------------------------------------------------------------*/}
                    <TouchableOpacity style={styles.SearchBtn}
                                      onPress={() => this.props.navigation.navigate('ShopFactors', {shopId: this.props.navigation.getParam('shopId')})}>
                        <Icon3 name={"cash-usd"} color={'#fff'} size={25}/>
                        <Text style={styles.BtnLabel}>{Dict['transactions']}</Text>
                    </TouchableOpacity>
                    {/*------My Orders------------------------------------------------*/}
                    <TouchableOpacity style={styles.SearchBtn}
                                      onPress={() => this.props.navigation.navigate('NewOrders', {
                                          shopId: this.props.navigation.getParam('shopId')
                                      })}>
                        <Icon name={"list"} color={'#fff'} size={25}/>
                        <Text style={styles.BtnLabel}>{Dict['neworders']}</Text>
                    </TouchableOpacity>
                    {/*------All Orders----------------------------------------------------*/}
                    <TouchableOpacity style={styles.SearchBtn}
                                      onPress={() => this.props.navigation.navigate('AllOrders', {
                                          shopId: this.props.navigation.getParam('shopId')
                                      })}>
                        <Icon name={"list-ol"} color={'#fff'} size={25}/>
                        <Text style={styles.BtnLabel}>{Dict['allorders']}</Text>
                    </TouchableOpacity>
                    {/*------SearchBtn------------------------------------------------*/}
                    <TouchableOpacity style={styles.SearchBtn}
                                      onPress={() => this.props.navigation.navigate('Search', {
                                          shopId: this.props.navigation.getParam('shopId')
                                      })}>
                        <Icon1 name={"search1"} color={'#fff'} size={32}/>
                    </TouchableOpacity>
                </View>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={5000}
                               containerStyle={{backgroundColor: "#eee"}}
                               titleStyle={Styles.TitleDropDown} messageStyle={Styles.BodyDropDown}/>
            </View>
        )
    }

    //---------------------GetTitles-------------------------------------------------------------------------------------
    _GetTitles(id) {
        this.setState({Empty: true});
        Connect.SendPRequest(URLS.Link() + "store", {shopId: parseInt(id)})
            .then(res => {
                if (res) {
                    this.setState({Titrs: res.cats, Empty: false});
                } else {
                    this.setState({Empty: false});
                }
            });
    }

    //---------------------GetCat-------------------------------------------------------------------------------------
    _GetCat(id, catId) {
        this.setState({Empty: true});
        Connect.SendPRequest(URLS.Link() + "store", {shopId: parseInt(id), catId: parseInt(catId)})
            .then(res => {
                if (res) {
                } else {
                    this.setState({Empty: false});
                }
            });
    }

}
