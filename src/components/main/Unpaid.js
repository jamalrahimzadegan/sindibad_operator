import {Component} from 'react';
import {AsyncStorage, Dimensions, View, Text, TouchableOpacity, FlatList} from 'react-native';
import React from 'react';
import DropdownAlert from 'react-native-dropdownalert';
import Styles from '../../assets/css/main/Styles';
import styles from '../../assets/css/main/MyOrders';
import Header from '../Header';
import URLS from '../../core/URLS';
import ListEmpty from '../ListEmpty';
import RenderShopsList from '../flatlists/RenderShopsList';
import {Connect} from '../../core/Connect';
import {connect} from 'react-redux';
import Dic from '../../core/Dic';

class Unpaid extends Component {
    constructor(props) {
        super(props);
        this.state = {
            HasDoing: false,
            entries: [],
            Unpaids: [],
        };
        this.Language = '';
    }

    componentDidMount() {
        this.props.navigation.addListener('willFocus', payload => {
            this._GetUnpaids();
        });
        AsyncStorage.getItem('language').then((lang) => {
            this.Language = lang;
            this.forceUpdate();
        });
    }

    render() {
        let Dict = Dic[this.Language === null || this.Language == '' ? 'Ku' : this.Language];
        return (
            <View style={styles.MainContianer}>
                {/*<CheckConnect/>*/}
                <Header navigation={this.props.navigation}/>
                <FlatList
                    renderItem={(item, index) => <RenderShopsList item={item} navigation={this.props.navigation}/>}
                    showsVerticalScrollIndicator={false}
                    data={this.state.Unpaids}
                    keyExtractor={(index) => index.toString()}
                    ListEmptyComponent={() => <ListEmpty
                        EmptyText={this.state.Empty ? Dict['loading'] : Dict['noitem']}
                        BgColor={'transparent'}/>}/>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={5000}

                               titleStyle={Styles.TitleDropDown} messageStyle={Styles.BodyDropDown}/>
            </View>
        );
    }

    //---------------------deliveryunpaid-------------------------------------------------------------------------------------
    _GetUnpaids() {
        Connect.SendPRequest(URLS.Link() + 'deliveryunpaid', {userId: parseInt(this.props.id)})
            .then(res => {
                // console.log('unpaids')
                // console.log(res)
                if (res) {
                    this.setState({Unpaids: res});
                }
            });
    }
}

//---------------------redux-------------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
    };
}

function mapDispatchToProps(dispatch) {
    // console.warn('dispatch: ',dispatch)
    return {
        getId: () => dispatch({type: 'getId'}),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Unpaid);