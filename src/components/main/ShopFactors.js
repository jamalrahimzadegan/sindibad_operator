import React from 'react';
import {
    FlatList,
    View,
    Text,
    Dimensions,
    AsyncStorage,
    Button, TouchableOpacity,
} from 'react-native';
import DropdownAlert from 'react-native-dropdownalert';
import {Connect} from '../../core/Connect';
import styles from '../../assets/css/main/ShopFactors';
import Header from '../Header';
import Styles, {AppColorRed} from '../../assets/css/main/Styles';
import ListEmpty from '../ListEmpty';
import URLS from '../../core/URLS';
import Dic from '../../core/Dic';
import {connect} from 'react-redux';
import RenderShopFactors from '../flatlists/RenderShopFactors';

class ShopFactors extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Factors: [],
            Empty: false,
        };
        this.Language = '';

    }

    componentWillMount() {
        AsyncStorage.multiGet([
            'language']).then((x) => {
            this.Language = x[0][1];
            this.forceUpdate();
        });
        this._GetFactors(this.props.navigation.getParam('shopId'));
    }

    render() {
        let Dict = Dic[this.Language === null || this.Language == '' ? 'Ku' : this.Language];
        return (
            <View style={styles.ShopFactorsContainer}>
                <Header navigation={this.props.navigation}/>
                <FlatList
                    style={{paddingVertical: 10, paddingBottom: 10}}
                    renderItem={(item) => <RenderShopFactors item={item} Dict={Dict}
                                                             navigation={this.props.navigation}
                                                             Language={this.Language}/>}
                    showsVerticalScrollIndicator={false}
                    // data={[1,2]}
                    data={this.state.Factors}
                    keyExtractor={(index, item) => index.toString()}
                    ListEmptyComponent={() => <ListEmpty
                        EmptyText={this.state.Empty ? Dict['loading'] : Dict['noitem']}
                        BgColor={'transparent'}/>}/>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={4000}

                               titleStyle={Styles.TitleDropDown} messageStyle={Styles.BodyDropDown}/>
            </View>
        );
    }

    //---------------------Getting Orders --------------------------------------------------------------------------------------
    _GetFactors(shopId) {
        this.setState({Empty: true});
        Connect.SendPRequest(URLS.Link() + 'factors', {shopId: parseInt(shopId)})
            .then(res => {
                if (res) {
                    this.setState({Factors: res, Empty: false});
                }
            }).catch((e) => {
            this.setState({Empty: false});
        });
    }
}

//---------------------redux-------------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
    };
}

function mapDispatchToProps(dispatch) {
    // console.warn('dispatch: ',dispatch)
    return {
        getId: () => dispatch({type: 'getId'}),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ShopFactors);