import {Component} from 'react';
import {AsyncStorage, Dimensions, View, Text, TextInput, TouchableOpacity, FlatList} from 'react-native';
import React from 'react';
import DropdownAlert from 'react-native-dropdownalert';
import Styles, {AppColorLightRed} from '../../assets/css/main/Styles';
import styles from '../../assets/css/main/Search';
import Header from '../Header';
import URLS from "../../core/URLS";
import {Connect} from "../../core/Connect";
import RenderSearchResult from "../flatlists/RenderSearchResult";
import Icon from 'react-native-vector-icons/AntDesign';
import ListEmpty from "../ListEmpty";
import {connect} from 'react-redux';
import Dic from '../../core/Dic';

 class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Empty: false,
            testRes: [],
            SearchText:'',
            Result: [],
        };
        this.Language = '';
    }
    componentDidMount() {
        this.props.navigation.addListener("willFocus", payload => {
            AsyncStorage.multiGet([
                'language']).then((x) => {
                this.Language = x[0][1];
                this.forceUpdate();
            });
            let shopId = this.props.navigation.getParam('shopId')
            let x = this.props.navigation.getParam('x')
            if(x!==undefined){
                this.setState({SearchText:x})
                this._Search(shopId,x)
            }
        });
    }

    render() {
        let shopId = this.props.navigation.getParam('shopId')
        let Dict = Dic[this.Language === null || this.Language == '' ? 'Ku' : this.Language];
        return (
            <View style={styles.MainContianer}>
                <Header navigation={this.props.navigation}/>
                <View style={styles.SearchBar}>
                    {/*-----------SearchBar----------------------------------------------------------------------------------------*/}
                    <TouchableOpacity style={styles.SearchBtn}
                                      onPress={() => this.props.navigation.navigate('Barcode', {
                                          RoutIs: 'Search',
                                          shopId:shopId,
                                          _ReadBarcode: (SearchText) => this.setState({SearchText},()=>this.forceUpdate())
                                      })}>
                        <Icon name={"barcode"} color={'#fff'} size={23} style={{}}/>
                    </TouchableOpacity>
                    <TextInput
                        onChangeText={(SearchText) => this._Search(shopId, SearchText,Dict)}
                        underlineColorAndroid={"transparent"}
                        value={this.state.SearchText}
                        // keyboardType={"number-pad"}
                        // returnKeyType={"done"}
                        placeholder={""}
                        style={styles.InputSearch}
                    />

                </View>
                <FlatList
                    renderItem={(item, index) => <RenderSearchResult item={item}
                                                                     shopId={shopId}
                                                                     navigation={this.props.navigation}/>}
                    showsVerticalScrollIndicator={false}
                    data={this.state.Result}
                    extraData={this.state.SearchText}
                    keyExtractor={(item, index) => index}
                    ListEmptyComponent={() => <ListEmpty
                        EmptyText={this.state.Empty ? Dict['loading'] : Dict['noitem']}
                        BgColor={'transparent'}/>}
                />
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={5000}
                               containerStyle={{backgroundColor: "red"}}
                               titleStyle={Styles.TitleDropDown} messageStyle={Styles.BodyDropDown}/>
            </View>
        )
    }

    //---------------------_Search-------------------------------------------------------------------------------------
    _Search(shopId, str,Dict) {
        this.setState({SearchText: str}, () => null)
        if (str.length > 2) {
            Connect.SendGRequest(URLS.Link() + `storesearch?shopId=${shopId}&str=${str}`)
                .then(res => {
                    if (res) {
                        this.setState({Result: res}, () => null)
                    }}).catch((e) => {
                this.dropdown.alertWithType('error', '', Dict['global_error']);
            });
        } else {
        }
    }

}
//---------------------redux-------------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
    };
}
function mapDispatchToProps(dispatch) {
    // console.warn('dispatch: ',dispatch)
    return {
        getId: () => dispatch({type: 'getId'}),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Search);