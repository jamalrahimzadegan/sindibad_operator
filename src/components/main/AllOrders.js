import {Component} from 'react';
import {AsyncStorage, Dimensions, View, Text, TouchableOpacity, FlatList} from 'react-native';
import React from 'react';
import DropdownAlert from 'react-native-dropdownalert';
import Styles from '../../assets/css/main/Styles';
import styles from '../../assets/css/main/MyOrders';
import Header from '../Header';
import URLS from '../../core/URLS';
import ListEmpty from '../ListEmpty';
import {Connect} from '../../core/Connect';
import RenderOrders from '../flatlists/RenderOrders';
import Dic from '../../core/Dic';

 export default class AllOrders extends Component {
    constructor(props) {
        super(props);
        this.state = {
            entries: [],
            AllOrders: [],
            Empty: false,
        };
        this.Language = '';
    }

    componentDidMount() {
        this.props.navigation.addListener('willFocus', payload => {
            AsyncStorage.multiGet([
                'language',
            ]).then((x) => {
                this.Language = x[0][1];
                this.forceUpdate();
                this._GetAllOrders();
            });
        });
    }
    render() {
        let Dict = Dic[this.Language === null || this.Language == '' ? 'Ku' : this.Language];
        return (
            <View style={styles.MainContianer}>
                <Header navigation={this.props.navigation}/>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={this.state.AllOrders}
                    keyExtractor={(index) => index.toString()}
                    renderItem={(item, index) => <RenderOrders
                        item={item}
                        navigation={this.props.navigation}/>}
                    ListEmptyComponent={() => <ListEmpty
                        EmptyText={this.state.Empty ? Dict['loading'] : Dict['noitem']}
                        BgColor={'transparent'}/>}/>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={5000}

                               titleStyle={Styles.TitleDropDown} messageStyle={Styles.BodyDropDown}/>
            </View>
        );
    }
    //---------------------Get All orders-------------------------------------------------------------------------------------
    _GetAllOrders() {
        this.setState({Empty: true});
        Connect.SendPRequest(URLS.Link() + 'shoporders', {shopId: parseInt(this.props.navigation.getParam('shopId'))})
            .then(res => {
                if (res) {
                    this.setState({AllOrders: res, Empty: false});
                } else {
                    this.setState({Empty: false});
                }
            });
    }
}
