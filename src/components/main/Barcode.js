import React, {Component} from 'react';
import {View, AsyncStorage, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {RNCamera} from 'react-native-camera';
import Icon from 'react-native-vector-icons/AntDesign';
import styles from "../../assets/css/main/Barcode";


export default class Barcode extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Barcode: ''
        }
    }

    render() {
        let RoutIs = this.props.navigation.getParam('RoutIs');
        let _ReadBarcode = this.props.navigation.getParam('_ReadBarcode');
        let shopId = this.props.navigation.getParam('shopId');
        return (
            <View style={styles.container}>
                <RNCamera
                    ref={ref => {
                        this.camera = ref;
                    }}
                    style={styles.preview}
                    type={RNCamera.Constants.Type.back}
                    flashMode={RNCamera.Constants.FlashMode.on}
                    androidCameraPermissionOptions={{
                        title: 'Permission to use camera',
                        message: 'We need your permission to use your camera',
                        buttonPositive: 'Ok',
                        buttonNegative: 'Cancel',
                    }}

                    onGoogleVisionBarcodesDetected={({barcodes}) => {
                        console.warn(barcodes[0].data)
                        this.setState({Barcode: barcodes[0].data},
                            () => {
                            _ReadBarcode(barcodes[0].data);
                        })
                    }}>
                    <Text
                        style={[styles.BarcodeText, {
                            left: this.state.Barcode !== '' ? 10 : 2000,
                            bottom: 10
                        }]}>{this.state.Barcode}</Text>
                </RNCamera>

                {/*-----------BarcodeBTN----------------------------------------------------------------------------------------*/}
                <TouchableOpacity style={[styles.SearchBtn, {bottom: this.state.Barcode === '' ? 3000 : 10}]}
                                  onPress={() =>RoutIs==='Search'? (this.props.navigation.replace(RoutIs, {x: this.state.Barcode,shopId:shopId})):this.props.navigation.goBack()}>
                    <Icon name={"back"} color={"#fff"} size={25} style={{}}/>
                </TouchableOpacity>
            </View>
        )

    }
    // takePicture = async () => {
    //     if (this.camera) {
    //         const options = {quality: 0.5, base64: true};
    //         const data = await this.camera.takePictureAsync(options);
    //         // console.log(data.uri);
    //     }
    // };
}



