import React from 'react';
import {
    FlatList,
    View,
    Text,
    AsyncStorage,
} from 'react-native';
import DropdownAlert from 'react-native-dropdownalert';
import {Connect} from '../../core/Connect';
import Header from '../Header';
import styles from '../../assets/css/main/TransactionList';
import Styles, {AppColorRed} from '../../assets/css/main/Styles';
import ListEmpty from '../ListEmpty';
import URLS from '../../core/URLS';
import Dic from '../../core/Dic';
import {connect} from 'react-redux';


class TransactionsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Factors: [],
            Empty: false,
        };
        this.Language = '';
    }

    componentWillMount() {
        this.props.navigation.addListener('willFocus', payload => {
            AsyncStorage.multiGet([
                'language']).then((x) => {
                    this.Language = x[0][1];
                    this._GetFactors();
                    this.forceUpdate();
                },
            );
        });
    }

    render() {
        // let Dict = Dic['Ar'];
        let Dict = Dic[this.Language === null || this.Language == '' ? 'Ku' : this.Language];

        return (
            <View style={styles.MainView}>
                <Header navigation={this.props.navigation}/>
                <FlatList
                    style={{paddingHorizontal: 10, paddingBottom: 10}}
                    renderItem={(item) => this._RenderFactors(item, Dict)}
                    showsVerticalScrollIndicator={false}
                    data={this.state.Factors}
                    keyExtractor={(index, item) => index.toString()}
                    ListEmptyComponent={() => <ListEmpty
                        EmptyText={this.state.Empty ? Dict['loading'] : Dict['noitem']}
                        BgColor={'transparent'}/>}/>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={4000}

                               titleStyle={Styles.TitleDropDown} messageStyle={Styles.BodyDropDown}/>
            </View>
        );
    }

    _RenderFactors(item, Dict) {
        return (
            <View style={{flex: 1, alignItems: this.Language == 'En' ? 'flex-start' : 'flex-end'}}>
                <View style={[styles.Container, {}]}>
                    <View style={{flexDirection: this.Language == 'En' ? 'row' : 'row-reverse'}}>
                        <Text style={[styles.eachFactorText, {display: item.item.Refid ? 'flex' : 'none'}]}>
                            {Dict['tracking_number']} {item.item.Refid}</Text>
                    </View>
                    <View style={{flexDirection: this.Language == 'En' ? 'row' : '.-reverse'}}>
                        <Text style={[styles.eachFactorText, {
                            display: item.item.AddCash ? 'flex' : 'none',
                        }]}>{Dict['increase']} {Connect.FormatNumber(parseInt(item.item.AddCash))}  </Text>
                    </View>
                    <View style={{flexDirection: this.Language == 'En' ? 'row' : 'row-reverse'}}>
                        <Text style={[styles.eachFactorText, {
                            display: item.item.DeCash ? 'flex' : 'none',
                        }]}>{Dict['decrease']} {Connect.FormatNumber(parseInt(item.item.DeCash))}  </Text>
                    </View>
                    <View style={{flexDirection: this.Language == 'En' ? 'row' : 'row-reverse'}}>
                        <Text style={styles.eachFactorText}>{Dict['fact_number']}</Text>
                        <Text style={styles.eachFactorText}>  {item.item.id}</Text>
                    </View>
                    <View style={{flexDirection: this.Language == 'En' ? 'row' : 'row-reverse'}}>
                        <Text
                            style={styles.eachFactorText}>{Dict['description']}: {this.Language == 'Fa' ? item.item.desc : this.Language == 'En' ? item.item.descEn : this.Language == 'Ar' ? item.item.descAr : this.Language == 'Ku' ? item.item.descKu : '-'}</Text>
                    </View>
                </View>
            </View>
        );
    }

    //---------------------Getting Orders --------------------------------------------------------------------------------------
    _GetFactors() {
        this.setState({Empty: true});
        Connect.SendPRequest(URLS.Link() + 'factors', {userId: parseInt(this.props.id)})
            .then(res => {
                // console.warn(res)
                if (res) {
                    this.setState({Factors: res, Empty: false});
                }
            }).catch((e) => {
            this.setState({Empty: false});
        });
    }
}

//---------------------redux-------------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
    };
}

function mapDispatchToProps(dispatch) {
    // console.warn('dispatch: ',dispatch)
    return {
        getId: () => dispatch({type: 'getId'}),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(TransactionsList);