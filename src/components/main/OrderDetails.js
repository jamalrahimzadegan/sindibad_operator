import {Component} from 'react';
import {AsyncStorage, ActivityIndicator, View, Text, ScrollView, TouchableOpacity, FlatList} from 'react-native';
import React from 'react';
import DropdownAlert from 'react-native-dropdownalert';
import Styles from '../../assets/css/main/Styles';
import styles from '../../assets/css/main/Details';
import Header from '../Header';
import ListEmpty from "../ListEmpty";
import {Connect} from "../../core/Connect";
import URLS from "../../core/URLS";
import OrderGoodsList from "../flatlists/OrderGoodsList";
import Dic from '../../core/Dic';
import {connect} from 'react-redux';

 class OrderDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Loading: false,
            Empty: false,
            Details: [],
            Orders: []
        };
        this.Language = '';
        this._GetDetails = this._GetDetails.bind(this)
    }

    componentWillMount() {
        AsyncStorage.multiGet([
            'language',
        ]).then((x) => {
            this.Language = x[0][1];
            this._GetDetails( this.props.navigation.getParam('OrderID'));
            this.forceUpdate();
        });
    }

    render() {
        let Dict = Dic[this.Language === null || this.Language == '' ? 'Ku' : this.Language];
        let {Details} = this.state;
        return (
            <View style={styles.DetailsContianer}>
                <Header navigation={this.props.navigation}/>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{alignItems: this.Language === 'En' ? 'flex-start' : 'flex-end', margin: 10}}>
                        <Text
                            style={styles.TitleDetailsText}>{Dict['customer']}: {Details.customerName !== null ? Details.customerName : '-'}</Text>
                        <Text
                            style={styles.TitleDetailsText}>{Dict['address']}: {Details.address !== null ? Details.address : '-'}</Text>
                        <Text
                            style={styles.TitleDetailsText}>{Dict['status']}: {Details.status !== null ? Details.status : '-'}</Text>
                        <Text
                            style={styles.TitleDetailsText}>{Dict['mobile_num']}: {Details.tell !== null ? Details.tell : '-'}</Text>
                        <Text
                            style={styles.TitleDetailsText}>{Dict['price']}: {Details.price !== null ? Connect.FormatNumber(parseInt(Details.price)) : '-'}</Text>
                    </View>
                    {/*-------Goods List---------------------------------------------------------------------------------*/}
                    <FlatList
                        renderItem={(item, index) => <OrderGoodsList
                            userId={this.state.ID}
                            orderId={this.props.navigation.getParam('OrderID')}
                            _GetDetails={this._GetDetails}
                            item={item}
                            confirmTime={Details.confirmTime == null ? null : Details.confirmTime}
                            beginTime={Details.beginTime == null ? null : Details.beginTime}
                            navigation={this.props.navigation}/>}
                        showsVerticalScrollIndicator={false}
                        data={this.state.Orders}
                        keyExtractor={(index) => index.toString()}
                        ListEmptyComponent={() => <ListEmpty
                            EmptyText={this.state.Empty ? Dict['loading'] : Dict['noitem']}
                            BgColor={'transparent'}/>}/>
                </ScrollView>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={5000}
                               containerStyle={{backgroundColor: "red"}}
                               titleStyle={Styles.TitleDropDown} messageStyle={Styles.BodyDropDown}/>
            </View>
        )
    }

    //---------------------GetDetails-------------------------------------------------------------------------------------
    _GetDetails( OrderId) {
        this.setState({Empty: true});
        Connect.SendPRequest(URLS.Link() + "operatororderdetail", {id: parseInt(OrderId), userId: parseInt(this.props.id)})
            .then(res => {
                if (res) {
                    this.setState({Details: res.request, Orders: res.orders[0].orderitems, Empty: false});
                } else {
                    this.setState({Empty: false});
                }
            });
    }

}
//---------------------redux-------------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
    };
}

function mapDispatchToProps(dispatch) {
    // console.warn('dispatch: ',dispatch)
    return {
        getId: () => dispatch({type: 'getId'}),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderDetails);