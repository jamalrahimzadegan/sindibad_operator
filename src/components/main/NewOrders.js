import {Component} from 'react';
import {AsyncStorage, Dimensions, View, Text, TouchableOpacity, FlatList} from 'react-native';
import React from 'react';
import DropdownAlert from 'react-native-dropdownalert';
import Styles from '../../assets/css/main/Styles';
import styles from '../../assets/css/main/MyOrders';
import Header from '../Header';
import URLS from '../../core/URLS';
import ListEmpty from '../ListEmpty';
import {Connect} from '../../core/Connect';
import RenderOrders from '../flatlists/RenderOrders';
import Dic from '../../core/Dic';

export default class NewOrders extends Component {
    constructor(props) {
        super(props);
        this.state = {
            entries: [],
            NewOrders: [],
            Empty: false,
        };
        this.Language = '';

    }

    componentDidMount() {
        this.props.navigation.addListener('willFocus', payload => {
            this._GetNewOrders();
        });
        AsyncStorage.multiGet([
            'language',
        ]).then((x) => {
            this.Language = x[0][1];
            this.forceUpdate();
        });
    }

    render() {
        let Dict = Dic[this.Language === null || this.Language == '' ? 'Ku' : this.Language];
        return (
            <View style={styles.MainContianer}>
                <Header navigation={this.props.navigation}/>
                <FlatList
                    renderItem={(item, index) => <RenderOrders
                        item={item}
                        navigation={this.props.navigation}/>}
                    showsVerticalScrollIndicator={false}
                    data={this.state.NewOrders}
                    keyExtractor={(index) => index.toString()}
                    ListEmptyComponent={() => <ListEmpty
                        EmptyText={this.state.Empty ? Dict['loading'] : Dict['noitem']}
                        BgColor={'transparent'}/>}/>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={5000}

                               titleStyle={Styles.TitleDropDown} messageStyle={Styles.BodyDropDown}/>
            </View>
        );
    }

    //---------------------Get New orders-------------------------------------------------------------------------------------
    _GetNewOrders() {
        this.setState({Empty: true});
        Connect.SendPRequest(URLS.Link() + 'shopneworders', {shopId: parseInt(this.props.navigation.getParam('shopId'))})
            .then(res => {
                if (res) {
                    this.setState({NewOrders: res, Empty: false});
                } else {
                    this.setState({Empty: false});
                }
            });
    }
}
