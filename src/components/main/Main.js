import {Component} from 'react';
import {AsyncStorage, Dimensions, View, Text, Image, TouchableOpacity, FlatList} from 'react-native';
import React from 'react';
import DropdownAlert from 'react-native-dropdownalert';
import Styles, {AppColorRed} from '../../assets/css/main/Styles';
import styles from '../../assets/css/main/Main';
import Header from '../Header';
import Carousel from 'react-native-snap-carousel';
import URLS from '../../core/URLS';
import ListEmpty from '../ListEmpty';
import RenderShopsList from '../flatlists/RenderShopsList';
import {Connect} from '../../core/Connect';
import {connect} from 'react-redux';
import Dic from '../../core/Dic';

const W = Dimensions.get('window').width;
const H = Dimensions.get('window').height;

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Empty: false,
            HasDoing: null,
            entries: [],
            Shops: [],
            MyYrders: [],
        };
        this.Language = '';


    }

    componentWillMount() {
        this.props.navigation.addListener('willFocus', payload => {
            AsyncStorage.multiGet([
                'language',
            ]).then((x) => {
                // this.props.setId(x[0][1])
                this.Language = x[0][1];
                this.forceUpdate();
                this._GetShops();
            });
        });
        // this.props.drawer_updateFn()
        this._GetSliders();
    }

    render() {
        let Dict = Dic[this.Language === null || this.Language == '' ? 'Ku' : this.Language];
        return (
            <View style={styles.MainContianer}>
                <Header navigation={this.props.navigation}/>
                <View style={{backgroundColor: AppColorRed}}>
                    <Carousel
                        data={this.state.entries}
                        renderItem={this._renderItem}
                        autoplay={true}
                        ref={(c) => this._carousel = c}
                        sliderWidth={W}
                        itemWidth={W}/>
                </View>
                <FlatList
                    renderItem={(item, index) => <RenderShopsList item={item}
                                                                  navigation={this.props.navigation}/>}
                    disableVirtualization={true}
                    columnWrapperStyle={{flexDirection: this.Language !== 'En' ? 'row-reverse' : 'row'}}
                    showsVerticalScrollIndicator={false}
                    // data={[1]}
                    data={this.state.Shops}
                    keyExtractor={(index) => index.toString()}
                    numColumns={2}
                    ListEmptyComponent={() => <ListEmpty
                        EmptyText={this.state.Empty ? Dict['loading'] : Dict['noitem']}
                        BgColor={'transparent'}/>}
                />
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={3000}
                               titleStyle={Styles.TitleDropDown} messageStyle={Styles.BodyDropDown}/>
            </View>
        );
    }

    //---Slider Render--------------------------------------------------------------------------------------------------
    _renderItem({item, index}) {
        return (
            <Image source={{uri: URLS.Slider() + item}}
                   style={styles.SliderImg}/>
        );
    }

    //---------------------_GetSliders--------------------------------------------------------------------------------------
    _GetSliders() {
        Connect.SendPRequest(URLS.Link() + 'slider', {})
            .then(res => {
                // console.warn(res)
                if (res) {
                    this.setState({entries: res});
                }
            });
    }


    //---------------------GetNewOrders-------------------------------------------------------------------------------------
    _GetShops() {
        this.setState({Empty: true});
        Connect.SendPRequest(URLS.Link() + 'operatorshop', {userId: parseInt(this.props.id)})
            .then(res => {
                if (res) {
                    this.setState({Shops: res, Empty: false});
                } else {
                    this.setState({Empty: false});

                }
            });
    }

}
//---------------------redux-------------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,
    };
}

function mapDispatchToProps(dispatch) {
    // console.warn('dispatch: ',dispatch)
    return {
        drawer_updateFn: () => dispatch({type: 'drawer_updateFn'}),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);