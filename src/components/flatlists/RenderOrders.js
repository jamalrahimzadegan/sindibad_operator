import {Component} from 'react';
import {AsyncStorage, ActivityIndicator, View, Text, ScrollView, TouchableOpacity, FlatList} from 'react-native';
import React from 'react';
import DropdownAlert from 'react-native-dropdownalert';
import Styles, {AppColorLightRed, AppColorRed} from '../../assets/css/main/Styles';
import styles from '../../assets/css/flat/RenderOrders';
import Dic from '../../core/Dic';


export default class RenderOrders extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Loading: false,
            Empty: false,
            Details: [],
            Orders: [],
        };
        this.Language = '';
    }
    componentWillMount() {
        AsyncStorage.multiGet([
            'language',
        ]).then((x) => {
            this.Language = x[0][1];
            this.forceUpdate();
        });
    }

    render() {
        let Dict = Dic[this.Language === null || this.Language == '' ? 'Ku' : this.Language];
        let item = this.props.item.item;
        var date = new Date(parseInt(item.createTime)*1000);
        return (
            <View style={styles.OrderBtnContainer}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('OrderDetails', {OrderID: item.id})}
                                  style={[styles.DetailBtn, {
                                      backgroundColor: this._OrderBtnColor(item),
                                  }]}>
                    <Text
                        style={styles.DetailsTexts}>{Dict['customer']}: {item.customerName ? item.customerName : '-'}</Text>
                    <Text style={styles.DetailsTexts}>{Dict['address']}: {item.address ? item.address : '-'}</Text>
                    <Text style={styles.DetailsTexts}>{Dict['mobile_num']}: {item.tell ? item.tell : '-'}</Text>
                    <View style={styles.OrderSeperator}/>
                    <Text style={[styles.DetailsTexts,{textAlign:'center'}]}>{date.toLocaleDateString()}  -  {date.toLocaleTimeString()}</Text>
                </TouchableOpacity>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={5000}

                               titleStyle={Styles.TitleDropDown} messageStyle={Styles.BodyDropDown}/>
            </View>
        );
    }


    _OrderBtnColor(item) {
        if (item.cancelTime > 0) {
            return AppColorRed;
        }
        if (item.adminDoneTime > 0) {
            return '#05a';
        }
        if (item.deliveryDoneTime > 0) {
            return '#218c74';
        }
        if (item.confirmTime > 0) {
            return '#fff';
        }
        return '#3dc1d3';

    }
}
