import {Component} from 'react';
import {AsyncStorage, View, Text, TouchableOpacity, FlatList, Button} from 'react-native';
import React from 'react';
import styles from '../../assets/css/flat/RenderGoods';
import {Connect} from '../../core/Connect';
import URLS from '../../core/URLS';
import Dic from '../../core/Dic';


export default class RenderGoods extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ShowDetails: false,
        };
        this.Language = '';

    }

    componentWillMount() {
        AsyncStorage.multiGet([
            'language',
        ]).then((x) => {
            this.Language = x[0][1];
            this.forceUpdate();
        });
    }


    render() {
        let {item} = this.props.item;
        let Dict = Dic[this.Language === null || this.Language == '' ? 'Ku' : this.Language];
        return (
            <TouchableOpacity
                style={[styles.CatchGiveBtn, {
                    alignSelf: this.Language !== 'En' ? 'flex-start' : 'flex-end',
                }]}
                onPress={() => this.props.navigation.navigate('ItemDetails', {
                    shopId: this.props.shopId,
                    item: item,
                })}>
                <Text style={[{}, styles.DetailsTexts]}>
                    {this.Language === 'En' ? item.titleEn : this.Language === 'Fa' ? item.title : this.Language === 'Ar' ? item.titleAr : this.Language === 'Ku' ? item.titleKu : '-'}
                </Text>
            </TouchableOpacity>
        );
    }
}
