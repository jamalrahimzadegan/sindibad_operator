import {Component} from 'react';
import {AsyncStorage, View, Text, TouchableOpacity, FlatList, Button, Platform, StyleSheet} from 'react-native';
import React from 'react';
import {Connect} from "../../core/Connect";
import URLS from "../../core/URLS";
import {AppColorLightRed, AppColorRed} from "../../assets/css/main/Styles";
import Dic from '../../core/Dic';


export default class OrderGoodsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ShowDetails: false,
        };
        this.Language = '';

    }

    componentWillMount() {
        AsyncStorage.multiGet([
            'language',

        ]).then((x) => {
            this.Language = x[0][1];
            this.forceUpdate();
            // console.warn(this.direction)
        });
    }

    render() {
        let Dict = Dic[this.Language === null || this.Language == '' ? 'Ku' : this.Language];
        let item = this.props.item.item;
        return (
            <View style={[styles.MainContianer, {
                flexDirection: this.Language !== 'En' ? 'row' : 'row-reverse',
            }]}>
                <TouchableOpacity
                    style={[styles.CatchGiveBtn, {backgroundColor: this._BtnColor(item)}]}
                    disabled={item.giveDeliveryTime !== null || item.cancelTime !== null }
                    onPress={() => this._btnAction(item,Dict)}>
                    <Text
                        style={[styles.DetailsTexts, {color: '#fff',}]}>{this._BtnLabel(item,Dict)}</Text>
                </TouchableOpacity>
                <Text
                    style={[styles.DetailsTexts,{flexShrink:1}]}>{this.Language === 'En' ? item.titleEn : this.Language === 'Fa' ? item.title : this.Language === 'Ar' ? item.titleAr : this.Language === 'Ku' ? item.titleKu : '-'}</Text>
            </View>
        )
    }

    //-----------Readyorder---------------------------------------------------------------------------------------------------------
    _Readyorder(id,Dict) {
        Connect.SendPRequest(URLS.Link() + "readyorder", {id: parseInt(id)})
            .then(res => {
                if (res === true) {
                    this.props._GetDetails(this.props.userId, this.props.orderId)
                } else {
                    this.dropdown.alertWithType('error', '', Dict['deliverError']);

                }
            });
    }

    //-----------Givedelivery---------------------------------------------------------------------------------------------------------
    _Givedelivery(id,Dict) {
        Connect.SendPRequest(URLS.Link() + "givedelivery", {id: parseInt(id)})
            .then((res) => {
                // console.warn('delivery: ' + res)
                if (res === true) {
                    this.props._GetDetails(this.props.userId, this.props.orderId)
                } else {
                    this.dropdown.alertWithType('error', '', Dict['deliverError']);
                }
            });
    }


    //-----------BtnLabel---------------------------------------------------------------------------------------------------------
    _BtnLabel(item,Dict) {
        if (item.operatorReadyTime == null && item.cancelTime == null) {
            return  Dict['good_ready']
        } else if (item.giveDeliveryTime == null && item.cancelTime == null) {
            return  Dict['good_delivered']
        } else {
            return  Dict['good_finished']
        }
    }

    //-----------BtnColor---------------------------------------------------------------------------------------------------------
    _BtnColor(item) {
        if (item.operatorReadyTime == null && item.cancelTime == null) {
            return '#00b16a'
        } else if (item.giveDeliveryTime == null && item.cancelTime == null&& this.props.beginTime>0) {
            return AppColorRed
        }
        else {
            return '#999'
        }
    }

    //-----------btnAction---------------------------------------------------------------------------------------------------------
    _btnAction(item,Dict) {
        if (item.operatorReadyTime == null && item.cancelTime == null) {
            this._Readyorder(parseInt(item.id),Dict)
        } else if (item.giveDeliveryTime == null && this.props.confirmTime > 0 && this.props.beginTime > 0 && item.cancelTime == null && item.operatorReadyTime > 0 ) {
            this._Givedelivery(parseInt(item.id),Dict)
        } else {

        }
    }
}

const styles = StyleSheet.create({

    MainContianer: {
        borderRadius: 4,
        marginVertical: 5,
        padding: 8,
        flex: 1,
        width: '96%',
        alignSelf: 'center',
        backgroundColor: '#666',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    DetailsTexts: {
        color: '#fff',
        fontSize: 14,
        fontFamily:'kurdishFont',
    },
    CatchGiveBtn: {
        padding: 8,
        marginHorizontal:5,
        borderRadius: 3,
        height: 35,
        alignItems: 'center',
        justifyContent: 'center'

    }
});
