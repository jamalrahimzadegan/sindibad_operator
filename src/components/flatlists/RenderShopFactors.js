import {Component} from 'react';
import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import styles from '../../assets/css/main/ShopFactors';
import {Connect} from '../../core/Connect';

export default class RenderShopFactors extends Component {
    constructor(props) {
        super(props);

    }
    render() {
        let {item,Dict}=this.props
        return (
            <View style={[styles.Container,{flexDirection: this.props.Language !== 'En' ?'row-reverse':'row'}]}>
                <View style={[{}]}>
                    <View style={{flexDirection: this.props.Language === 'En' ? 'row' : 'row-reverse'}}>
                        <Text style={styles.eachFactorText}>{Dict['fact_number']}</Text>
                        <Text style={styles.eachFactorText}>  {item.item.id}</Text>
                    </View>
                    <View style={{flexDirection: this.props.Language === 'En' ? 'row' : 'row-reverse'}}>
                        <Text style={[styles.eachFactorText, {
                            display: item.item.AddCash ? 'flex' : 'none',
                        }]}>{Dict['trans_inmoney']} {Connect.FormatNumber(parseInt(item.item.AddCash))}  </Text>
                    </View>
                    <View style={{flexDirection: this.props.Language === 'En' ? 'row' : 'row-reverse'}}>
                        <Text style={[styles.eachFactorText, {
                            display: item.item.DeCash ? 'flex' : 'none',
                        }]}>{Dict['trans_decmoney']} {Connect.FormatNumber(parseInt(item.item.DeCash))}  </Text>
                    </View>
                    <View style={{flexDirection: this.props.Language === 'En' ? 'row' : 'row-reverse'}}>
                        <Text style={[styles.eachFactorText, {display: item.item.Refid ? 'flex' : 'none'}]}>
                            {Dict['tracking_number']}: {item.item.Refid}</Text>
                    </View>
                </View>
                <TouchableOpacity style={styles.DetailBTN}
                                  onPress={() => this.props.navigation.navigate('OrderDetails', {OrderID: item.item.orderId})}>
                    <Text style={[styles.eachFactorText, {
                        color: '#333',
                    }]}>{Dict['description']}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
