import {Component} from 'react';
import {AsyncStorage, View, Text, TouchableOpacity, FlatList, Button, StyleSheet, Platform} from 'react-native';
import React from 'react';
import {AppColorRed} from '../../assets/css/main/Styles';
import Dic from '../../core/Dic';


export default class RenderSearchResult extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ShowDetails: false,
        };
        this.Language = '';
        AsyncStorage.multiGet([
            'language',
        ]).then((x) => {
            this.Language = x[0][1];
            this.forceUpdate();
        });
    }

    render() {
        let item = this.props.item.item;
        return (
            <TouchableOpacity onPress={() => this.props.navigation.navigate('ItemDetails', {
                shopId: this.props.shopId,
                item: item,
            })} style={styles.ItemButton}>
                <Text
                    style={styles.TitleText}>{this.Language === 'En' ? item.titleEn : this.Language === 'Fa' ? item.title : this.Language === 'Ar' ? item.titleAr : this.Language === 'Ku' ? item.titleKu : '-'}</Text>
            </TouchableOpacity>
        );
    }
}

export const styles = StyleSheet.create({
    ItemButton: {
        width: '90%',
        alignSelf: 'center',
        height: 50,
        borderRadius: 5,
        backgroundColor: AppColorRed,
        padding: 10,
        marginVertical: 3,
        justifyContent: 'center',
        ...Platform.select({
            ios: {
                boxWithShadow: {
                    shadowColor: '#000',
                    shadowOffset: {width: 0, height: 1},
                    shadowOpacity: 0.8,
                    shadowRadius: 1,
                },
            },
            android: {
                elevation: 5,
            },
        }),
    },
    TitleText: {
        color: '#fff',
        fontSize: 15,
        fontFamily: 'kurdishFont',

    },
});
