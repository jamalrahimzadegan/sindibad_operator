import {Component} from 'react';
import {AsyncStorage, View, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import DropdownAlert from 'react-native-dropdownalert';
import Styles from '../../assets/css/main/Styles';
import styles from '../../assets/css/flat/RenderShopsList';
import FastImage from 'react-native-fast-image';
import URLS from '../../core/URLS';
import Dic from '../../core/Dic';

export default class RenderShopsList extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.language = '';

    }

  componentWillMount() {
      AsyncStorage.multiGet([
          'language',
      ]).then((x) => {
          // this.props.setId(x[0][1])
          this.language = x[0][1];
          this.forceUpdate();
      });
  }

    render() {
        let {item} = this.props.item;
        return (
            <View style={[styles.RenderListOrderContianer, {}]}>
                <TouchableOpacity
                    style={[styles.ListBtnContainer, {flexDirection: this.language == 'En' ? 'row' : 'row-reverse'}]}
                    onPress={() => this.props.navigation.navigate('ShopItems', {shopId: item.id})}>
                    {
                        item.image ?
                            <FastImage source={{uri: item.image ? URLS.Media() + item.image : null}}
                                       style={styles.ShopImg}/> :
                            <FastImage source={require('./../../assets/images/online-store.png')}
                                       style={styles.ShopImg}/>
                    }
                    <Text
                        style={styles.TextTitle}>{this.language == 'En' ? item.titleEn : this.language == 'Fa' ? item.title : this.language == 'Ar' ? item.titleAr : this.language == 'Ku' ? item.titleKu : null}</Text>
                </TouchableOpacity>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={5000}

                               titleStyle={Styles.TitleDropDown} messageStyle={Styles.BodyDropDown}/>
            </View>
        );
    }
}
