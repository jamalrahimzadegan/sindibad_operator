import {Component} from 'react';
import {AsyncStorage, View, Text, TouchableOpacity, FlatList, Button} from 'react-native';
import React from 'react';
import styles from '../../assets/css/flat/RenderTitrs';
import ListEmpty from '../ListEmpty';
import RenderGoods from './RenderGoods';
import Dic from '../../core/Dic';


export default class RenderTitrs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ShowCats: false,
        };
        this.Language = '';
        this.Catched = false;
    }

    componentWillMount() {
        AsyncStorage.multiGet([
            'language',
        ]).then((x) => {
            this.Language = x[0][1];
            this.forceUpdate();
        });
    }

    render() {
        let item = this.props.item.item.cat;
        let Data = this.props.item.item.product;
        let Dict = Dic[this.Language === null || this.Language == '' ? 'Ku' : this.Language];

        return (
            <View style={styles.MainContianer}>
                <TouchableOpacity onPress={() => this.setState({ShowCats: !this.state.ShowCats})}
                                  style={[{}, styles.MarketButton]}>
                    <Text
                        style={[{textAlign: this.Language === 'En' ? 'left' : 'right'}, styles.TitrText]}>{this.Language === 'En' ? item.titleEn : this.Language === 'Fa' ? item.title : this.Language === 'Ar' ? item.titleAr : this.Language === 'Ku' ? item.titleKu : '-'}</Text>
                </TouchableOpacity>
                {
                    this.state.ShowCats ?
                        <View style={styles.ShopDetails}>
                            <FlatList
                                style={{marginBottom: 15}}
                                renderItem={(item, index) => <RenderGoods
                                    shopId={this.props.shopId}
                                    item={item}
                                    navigation={this.props.navigation}
                                />}
                                showsVerticalScrollIndicator={false}
                                data={Data}
                                listKey={(item, index) => 'g' + index.toString()}
                                ListEmptyComponent={() => <ListEmpty
                                    EmptyText={Dict['noitem']}
                                    BgColor={'transparent'}/>}/>
                        </View>
                        : null
                }
            </View>
        );
    }
}
