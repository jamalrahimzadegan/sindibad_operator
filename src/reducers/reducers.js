import {AsyncStorage} from 'react-native';
import {createStore} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';


//---------------------------------initilState----------------------------------------------------------------------------
export const initilState = {
    id: null,
    Language: null,
    drawer_update: 0,
};
//---------------------------------reducers----------------------------------------------------------------------------
const reducer = (state = initilState, action) => {
    switch (action.type) {
        case 'setId':
            return {...state, id: action.payload.id};
        case 'drawer_updateFn':
            return {...state, drawer_update: state.drawer_update + 1};
        case 'PURGE':
            return {id:null,drawer_update: 0};

    }
    return state;
};
//---------------------------------persistConfig----------------------------------------------------------------------------
export const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
};
//---------------------------------persistedReducer----------------------------------------------------------------------------
export const persistedReducer = persistReducer(persistConfig, reducer);
//---------------------------------store----------------------------------------------------------------------------
export const store = createStore(persistedReducer);
//---------------------------------persistor----------------------------------------------------------------------------
export const persistor = persistStore(store);
