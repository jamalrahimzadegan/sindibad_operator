import React, {Component} from 'react';
import { AsyncStorage} from 'react-native';
import { createDrawerNavigator, createStackNavigator} from 'react-navigation';
import Splash from './components/Splash';
import Login from './components/main/Login';
import Main from './components/main/Main';
import Search from './components/main/Search';
import ShopItems from './components/main/ShopItems';
import Profile from './components/main/Profile';
import ItemDetails from './components/main/ItemDetails';
import TransactionsList from './components/main/TransactionsList';
import RenderGoods from './components/flatlists/RenderGoods';
import AllOrders from './components/main/AllOrders';
import NewOrders from './components/main/NewOrders';
import ShopFactors from './components/main/ShopFactors';
import OrderDetails from './components/main/OrderDetails';
import Barcode from './components/main/Barcode';
import {PersistGate} from 'redux-persist/integration/react';
import {persistor, store} from './reducers/reducers';
import {connect, Provider} from 'react-redux';
import DrawerStyle from './components/DrawerStyle';

//---------First Navigator------------------------------------------------------------------------------------
const AppNavigator = createStackNavigator({
        Splash: {screen: Splash, navigationOptions: {header: null}},
        Login: {screen: Login, navigationOptions: {header: null}},
        Main: {screen: Main, navigationOptions: {header: null}},
        ShopFactors: {screen: ShopFactors, navigationOptions: {header: null}},
        OrderDetails: {screen: OrderDetails, navigationOptions: {header: null}},
        Search: {screen: Search, navigationOptions: {header: null}},
        Barcode: {screen: Barcode, navigationOptions: {header: null}},
        ShopItems: {screen: ShopItems, navigationOptions: {header: null}},
        AllOrders: {screen: AllOrders, navigationOptions: {header: null}},
        NewOrders: {screen: NewOrders, navigationOptions: {header: null}},
        RenderGoods: {screen: RenderGoods, navigationOptions: {header: null}},
        ItemDetails: {screen: ItemDetails, navigationOptions: {header: null}},
        Profile: {screen: Profile, navigationOptions: {header: null}},
        TransactionsList: {screen: TransactionsList, navigationOptions: {header: null}},
    },
    {
        // initialRouteName: 'BarcodeTest',
    });
//---------Drawer Navigator------------------------------------------------------------------------------------
const Drawer = createDrawerNavigator({
    Home: {screen: AppNavigator, navigationOptions: {header: null}},
}, {
    drawerWidth: 275,
    drawerPosition: 'right',
    contentComponent: ({navigation}) => (<DrawerStyle navigation={navigation}/>),

});
//---------Drawer Navigator En------------------------------------------------------------------------------------
const DrawerEn = createDrawerNavigator({
    Home: {screen: AppNavigator, navigationOptions: {header: null}},
}, {
    drawerWidth: 270,
    drawerPosition:'left',
    contentComponent: ({navigation}) => (<DrawerStyle navigation={navigation}/>),
});

 export default class App extends Component {
    constructor(props) {
        super(props);
        this.Lang = '';
        // AsyncStorage.clear();
        // AsyncStorage.removeItem('language')
        AsyncStorage.getItem('language').then((lan) => {
            if (lan) {
                // console.warn(lan)
                this.Lang = lan;
                this.forceUpdate();
            }
        });
    }

    render() {

        if (this.Lang === 'En') {
            return (
                <Provider store={store}>
                    <PersistGate loading={null} persistor={persistor}>
                        <DrawerEn/>
                    </PersistGate>
                </Provider>
            );
        } else {
            return (
                <Provider store={store}>
                    <PersistGate loading={null} persistor={persistor}>
                        <Drawer/>
                    </PersistGate>
                </Provider>
            );
        }
    }
}
