

export default
    {
        Ku: {
            //-------LOGIN AND SIGUP--------------------------------------------------------------------------------------------------------
            't_login_sms': 'تکایا ئەو کۆدەی بۆت هاتووە داخڵی بکە',
            't_login_login': 'چوونەژوورەوە',
            'agree_rules': ' ڕازیم بە یاساکان',
            'agree_rules': 'ڕازیم بە یاساکان',
            't_login_wrongnumber': 'تکایا ژمارە موبایلەکەت بە دروستی داخڵ بکە',
            't_login_wrongcode': 'تکایا ئەو کۆدەی بۆت هاتووە بە دروستی داخڵی بکە',
            'login_noname': 'تکایا ناوی خۆت داخڵ بکە',
            'login_nofamily': 'تکایا ناوی باوک داخڵ بکە',
            'singup_serverError': 'تۆمارکردنەکەت کێشەی هەیە لەلایەن سێرڤەرەوە',
            'singup_globalerror': 'کیشەیەک هەیە لە تۆمار کردنى زمارەکەت  تکایە ت کەمیکى تر دوبارەى بکەرەوە',
            'singup_isUser': 'بەریزتان پیشتر بە ناونیشانی دیکە تۆمارت کردبوو بۆ کۆرینى ناونیشانت  تکایە بەوەندى بکە بە  کول سنەترەوە بکە بۆ ئەنجامدانى داواکاریەکەت',
            //-------TRANSACTIONS-----------------------------------------------------------------------------------------------------------------------------------
            't_req_notransaction': 'بەریزتان چالاکیتان نیە لە ئیستادا',
            'fact_number': 'ژمارەى پسولە:',
            'trans_inmoney': 'زیاد كردنةوةى ئةژمارةكةت:',
            'trans_decmoney': 'كةم كردنةوةى ئةژمارةكةت:',
            'tracking_number': 'کودى  بە دواچوون',
            //-------DRAWER-------------------------------------------------------------------------------------------------------------------------------------
            'change_language': 'گۆڕینی زمان',
            't_profile_logout': 'دەرچوون',
            'increase_money': 'زياد كردنى  ثارةى ئةژمار',
            'edit_profile': 'پرۆفایل',
            'transactions': 'چالاکیە دارایەکان',
            'moraef_code': 'كؤدى ناساندن',
            'rank': 'خال:',
            'cash': 'حسابى ماوەتان',
            'logout_text': 'ئایا بەریزت دلنیایت لە دەرچوون؟',
            //-------DETAILS-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            'deliver_finishing': 'کۆتایى',
            'searchitem': 'گەران',
            'allorders': 'هەموو داواکاریەکان',
            'neworders': 'داواکارى نوێ',
            'addgood': 'تۆمار کردنى کاڵا',
            'customer': 'كريار',
            'nobarcode': 'یەکەم جار بارکودى کاڵا  تۆمار بکە',
            'price': 'نرخ',
            'good_ready': 'ئاماده',
            'good_delivered': 'گەیاندن',
            'good_finished': 'تەواو بووە',
            'remfromstroe': 'کەم کردن له کۆگا',
            'addtostore': 'زیاد کردن به کۆگا',
            'discount_percent': 'داشکاندن (%)',
            'deliverError': 'هەلەیەک روى دا لە تۆمار  کردنى زانیاریەکانت تکایە دوبارەى بکەرەوە',
            'count': 'ژمار',
            'decrease': 'که م کردن',
            'increase': 'زيادکردن',
            //-------PROFILE----------------------------------------------------------------------------------------------------------------------------------------------------------
            't_profile_codemeli': 'ژمارەى کارتى نیشتیمانى',
            'success_photo': 'وینەکەت بەسەرکەوتووى ئەبلود بوو دواى تۆمار کردنەکەت  ئەنجام دەدرێت',
            't_profile_name': 'ناو',
            't_profile_family': 'ناوى باو و باپیر',
            't_profile_company': 'ناوى کۆمپانیا',
            'succsses_update': 'زانیاریەکانت بە سەرکەوتویی تۆمار کران',
            'faild_update': 'هەلەیەک روى دا لە تۆمار  کردنى زانیاریەکانت تکایە دوبارەى بکەرەوە',
            'select_city': 'تکایە  شارى خۆت هەلبژێرە',
            'city_city': 'شار',
            'province': 'پاریزگا',
            //-------------Global (errors+Messages+alerts)---------------------------------------------------------------------------------------------------------------------------------------------------------------------
            't_main_confirm': 'تاكيد كردن',
            't_main_cancel': 'داخستن',
            'loading': 'زانیاریەکان  خةريكة داونلود  دەبن ...',
            'noitem': 'هیچ شتیک لە ئیستادا بە ردەست نیە',
            'description': 'تێبینى',
            'imagepicker_Title': 'هەلبژاردنى وینە',
            'imagepicker_internal': 'داخلی موبایل',
            'imagepicker_camera': 'كاميرا',
            'imagepicker_cancel': 'داخستن',
            'status': 'رةوش',
            'server_error': 'کیشە هەیە  لە ئەنتەرنیتەکەدا',
            'global_error': 'تکایە دوبارە هەولبدەرەوە',
            'confirmed': 'تؤمار كرا',
            'new_version': 'فێرژنى نوى',
            'new_version_download': 'دابەزاندنى  فێرژنى نوى',
            'address': 'ناونيشان',
            'phone_num': 'ژمارەى هیلى  نۆرمال',
            'mobile_num': 'ژمارەى موبايل',
            'closing': 'داخستن',
        },
        En: {
            //-------LOGIN AND SIGUP--------------------------------------------------------------------------------------------------------
            't_login_sms': 'Please enter recieved sms',
            't_login_login': 'Login',
            'agree_rules': 'Accept Rules',
            'alert_rules': 'Please accept the rules to enter.',
            't_login_wrongnumber': 'Your phone number is not correct',
            't_login_wrongcode': 'Please enter code correctly',
            'login_noname': 'Please enter your name',
            'login_nofamily': 'Please enter your last name',
            'singup_serverError': 'Server registration problem (code 3)',
            'singup_globalerror': 'Problem registering the number, please try later (code 1)',
            'singup_isUser': 'You cannot login as a courier Please contact support.',
            //-------MAIN------------------------------------------------------------------------------------------------------------------------------------------------------------
            'my_orders': 'My Orders',
            'not_sattled': 'Not Sattled',
            'doing_button': 'In progress',
            'doing_message': 'You have no orders in progress',
            //-------TRANSACTIONS-----------------------------------------------------------------------------------------------------------------------------------
            't_req_notransaction': 'There is no transaction',
            'fact_number': 'Fact number:',
            'trans_inmoney': 'Increase inventory:',
            'trans_decmoney': 'Reduce inventory:',
            'tracking_number': 'Tracking Number',
            //-------DRAWER-------------------------------------------------------------------------------------------------------------------------------------
            'change_language': 'Language',
            't_profile_logout': 'Logout',
            'increase_money': 'Increase credit',
            'edit_profile': 'Profile',
            'transactions': 'Transactions',
            'moraef_code': 'Reagent code',
            'rank': 'Points:',
            'cash': 'Inventory',
            'logout_text': 'Do you want to sign out of your account?',
            //-------ItemDetails-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            'searchitem': 'Search',
            'allorders': 'All orders',
            'neworders': 'New Orders',
            'addgood': 'Product registration',
            'deliverError': 'Error in deliver',
            'customer': 'Customer',
            'nobarcode': 'Enter the product barcode first',
            'price': 'Price',
            'good_finished': 'Finished',
            'good_ready': 'Ready',
            'good_delivered': 'Deliver',
            'remfromstroe': 'decrease from \n Warehouse',
            'addtostore': 'Add to \n Warehouse',
            'discount_percent': 'Discount percent',
            'count': 'count',
            'decrease': 'Decrease',
            'increase': 'Increase',
            //-------PROFILE----------------------------------------------------------------------------------------------------------------------------------------------------------
            't_profile_codemeli': 'National Code',
            'success_photo': 'The image has been uploaded successfully and will be changed after you hit the Confirm button',
            't_profile_name': 'Name',
            't_profile_family': 'Last name',
            't_profile_company': 'Company',
            'succsses_update': 'Information updated successfully.',
            'faild_update': 'Error registering information, please try again.',
            'select_city': 'Please choose your city',
            'city_city': 'City',
            'province': 'Province',
            //-------------Global (errors+Messages+alerts)---------------------------------------------------------------------------------------------------------------------------------------------------------------------
            't_main_confirm': 'Confrim',
            't_main_cancel': 'Cancel',
            'loading': 'Getting information ...',
            'noitem': 'There is no item',
            'description': 'Description',
            'imagepicker_Title': 'Pick Image',
            'imagepicker_internal': 'Internal Storage',
            'imagepicker_camera': 'Camera',
            'imagepicker_cancel': 'Cancel',
            'status': 'Status',
            'server_error': 'Server error',
            'global_error': 'Please try again',
            'confirmed': 'Confirmed',
            'new_version': 'New version available',
            'new_version_download': 'Download new version',
            'address': 'Address',
            'phone_num': 'Phone',
            'mobile_num': 'Mobile',
            'closing': 'Close',
        },
        Ar: {
            //-------LOGIN AND SIGUP--------------------------------------------------------------------------------------------------------
            't_login_sms': 'الرجاء إدخال الرمز الذي أرسلناه',
            't_login_login': 'تسجيل الدخول',
            'agree_rules': ' أتفق مع سياسة الموقع ',
            'alert_rules': 'يرجى قبول سياسة الموقع للدخول.',
            't_login_wrongnumber': 'الرجاء إدخال رقم الهاتف بشكل صحيح',
            't_login_wrongcode': 'الرمز غير صحيح ، امسح الرمز وأعد الكتابة',
            'login_noname': 'من فضلك ادخل اسمك',
            'login_nofamily': 'رجاء إدخل  اللقب',
            'singup_serverError': 'مشكلة تسجيل الخادم (الكود 3)',
            'singup_globalerror': 'مشكلة في تسجيل الرقم، يرجى المحاولة لاحقًا (الكود 1)',
            'singup_isUser': 'لا يمكنك تسجيل الدخول كبريد، يرجى الاتصال بالدعم.',
            //-------TRANSACTIONS-----------------------------------------------------------------------------------------------------------------------------------
            't_req_notransaction': 'لا يوجد معاملة',
            'fact_number': 'رقم الفاتورة:',
            'trans_inmoney': 'زيادة ائتمان:',
            'trans_decmoney': 'تقليل ائتمان:',
            'tracking_number': 'رقم التتبع',
            //-------DRAWER-------------------------------------------------------------------------------------------------------------------------------------
            'change_language': 'اللغة',
            't_profile_logout': 'الخروج',
            'increase_money': 'زيادة الائتمان',
            'edit_profile': 'بروفایل',
            'transactions': 'المعاملات',
            'moraef_code': 'کود المعرف',
            'rank': 'النقاط:',
            'cash': 'النقدية',
            'logout_text': 'هل ترغب  بالخروج ؟',
            //-------ItemDetails-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            'searchitem': 'بحث',
            'allorders': 'جميع الطلبات',
            'neworders': 'طلبات جديدة',
            'addgood': 'تسجيل المنتج',
            'customer': 'زبون',
            'nobarcode': 'أدخل الرمز الشريطي للمنتج أولاً',
            'price': 'السعر',
            'good_ready': 'استعداد',
            'good_delivered': 'تسليم',
            'good_finished': 'انتهى',
            'remfromstroe': 'تقليل من المستودع',
            'addtostore': 'أضف إلى المستودع',
            'discount_percent': 'نسبة الخصم',
            'deliverError': 'خطأ في التحضير والتسليم',
            'count': 'العدد',
            'decrease': 'انخفاض',
            'increase': 'زيادة',
            //-------PROFILE----------------------------------------------------------------------------------------------------------------------------------------------------------
            't_profile_codemeli': 'کود الوطني',
            'success_photo': 'تم تحميل الصورة بنجاح وسيتم تغييرها بعد الضغط على زر التأكيد',
            't_profile_name': 'الاسم الاول',
            't_profile_family': 'الاسم الاخير',
            't_profile_company': 'اسم الشركة',
            'succsses_update': 'تم تحديث المعلومات بنجاح.',
            'faild_update': 'خطأ في تسجيل المعلومات ، يرجى المحاولة مرة أخرى.',
            'select_city': 'الرجاء اختيار مدينتكد',
            'city_city': 'المدينة',
            'province': 'المحافظه',
            //-------------Global (errors+Messages+alerts)---------------------------------------------------------------------------------------------------------------------------------------------------------------------
            't_main_confirm': 'التأكيد',
            't_main_cancel': 'إلغاء',
            'loading': 'جار الحصول على البیانات ...',
            'noitem': 'لا یوجد شی',
            'description': 'تفاصيل',
            'imagepicker_Title': 'اختر صورة',
            'imagepicker_internal': 'الذاكرة الداخلية',
            'imagepicker_camera': 'الکامیرا',
            'imagepicker_cancel': 'إلغاء',
            'status': 'الحالة',
            'server_error': 'خطأ في الاتصال مع الخادم',
            'global_error': 'لطفا دوباره تلاش کنید',
            'confirmed': 'تم التأكيد',
            'new_version': 'نسخة جديدة متاحة',
            'new_version_download': 'تحميل نسخة جديدة',
            'address': 'العنوان',
            'phone_num': 'هاتف',
            'mobile_num': 'رقم الجوال',
            'closing': 'اغلاق',
        },
        Fa: {
            //-------LOGIN AND SIGUP-------------------------------------------------------------------------------------------------------------------------------------------------------
            'agree_rules': 'با قوانین موافقم',
            'alert_rules': 'لطفا برای ورود با قوانین موافقت کنید.',
            't_login_login': 'ورود',
            't_login_sms': 'لطفا کد ارسال شده را وارد کنید',
            'login_noname': 'لطفا نام خود را وارد کنید',
            'login_nofamily': 'لطفا نام خانوادگی خود را وارد کنید',
            't_login_wrongnumber': 'لطفا شماره تلفن را به درستی وارد کنید',
            't_login_wrongcode': 'لطفا کد ارسال شده را درست وارد کنید',
            'singup_serverError': 'مشکل ثبت نام از طرف سرور( کد ۳)',
            'singup_globalerror': 'مشکل در ثبت شماره، لطفا بعدا امتحان کنید(کد ۱)',
            'singup_isUser': 'شما امکان ورود ندارید، با پشتیبانی تماس بگیرید.',
            //-------MAIN------------------------------------------------------------------------------------------------------------------------------------------------------------
            'my_orders': 'درخواست های من',
            'not_sattled': 'تسویه نشده',
            'doing_button': 'در حال انجام',
            'doing_message': 'شما سفارش در حال انجامی ندارید',
            //-------TRANSACTIONS-----------------------------------------------------------------------------------------------------------------------------------
            't_req_notransaction': 'تراکنشی وجود ندارد',
            'fact_number': 'شماره فاکتور:',
            'trans_inmoney': 'افزایش موجودی:',
            'trans_decmoney': 'کاهش موجودی:',
            'tracking_number': 'شماره پیگیری',
            //-------DRAWER-------------------------------------------------------------------------------------------------------------------------------------
            'change_language': 'تغییر زبان',
            't_profile_logout': 'خروج',
            'increase_money': 'افزایش اعتبار',
            'edit_profile': 'پروفایل',
            'transactions': 'تراکنش ها',
            'moraef_code': 'کد معرف',
            'rank': 'امتیاز:',
            'cash': 'موجودی',
            'logout_text': 'آیا مایل به خروج از حساب کاربری خود می باشید؟',
            //-------ItemDetails-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            'searchitem': 'جستجو',
            'allorders': 'همه سفارشات',
            'neworders': 'سفارشات جدید',
            'addgood': 'ثبت محصول',
            'customer': 'مشتری',
            'nobarcode': 'ابتدا بارکد محصول را وارد کنید',
            'price': 'قیمت',
            'good_ready': 'آماده است',
            'good_delivered': 'تحویل',
            'good_finished': 'پایان یافته',
            'remfromstroe': 'کاهش از انبار',
            'addtostore': 'اضافه به انبار',
            'discount_percent': 'درصد تخفیف',
            'deliverError': 'خطا در آماده سازی و تحویل',
            'count': 'تعداد',
            'decrease': 'کاهش',
            'increase': 'افزایش',
            //-------PROFILE----------------------------------------------------------------------------------------------------------------------------------------------------------
            't_profile_codemeli': 'کد ملی',
            'success_photo': 'تصویر با موفقیت آپلود شد و بعد از زدن دکمه ثبت اطلاعات، تغییر خواهد یافت',
            't_profile_name': 'نام',
            't_profile_family': 'نام خانوادگی',
            't_profile_company': 'نام شرکت',
            'succsses_update': 'اطلاعات با موفقیت آپدیت شدند.',
            'faild_update': 'خطا در ثبت اطلاعات، لطفا دوباره تلاش کنید.',
            'select_city': 'لطفا شهر خود را انتخاب کنید',
            'city_city': 'شهر',
            'province': 'استان',
            //-------------Global (errors+Messages+alerts)---------------------------------------------------------------------------------------------------------------------------------------------------------------------
            't_main_confirm': 'تایید',
            't_main_cancel': 'انصراف',
            'loading': 'در حال دریافت اطلاعات...',
            'noitem': 'موردی وجود ندارد',
            'description': 'توضیحات',
            'imagepicker_Title': 'انتخاب عکس',
            'imagepicker_internal': 'حافظه داخلی',
            'imagepicker_camera': 'دوربین',
            'imagepicker_cancel': 'انصراف',
            'status': 'وضعیت',
            'server_error': 'خطا در ارتباط با سرور',
            'global_error': 'لطفا دوباره تلاش کنید',
            'confirmed': 'تایید شد',
            'new_version': 'نسخه جدید موجود است',
            'new_version_download': 'دانلود نسخه جدید',
            'address': 'آدرس',
            'phone_num': 'تلفن ثابت',
            'mobile_num': 'تلفن همراه',
            'closing': 'بستن',
        },
    }
;

