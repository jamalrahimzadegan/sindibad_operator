export default class URLS {
    static SiteAddress(){
        return Host+"/";
    }
    static Link(){
        return Host+"/admin/mobile/";
    }
    static Media(){
        return Host+"/upload/";
        // return Host+"/admin/mobile/upload/";
    }
    static Slider(){
        return Host+"/upload/slider/";

    }
    static MediaSmall(){
        return Host+"/upload/smallthumbnail/";
    }
    static MediaThumbnail(){
        return Host+"/upload/thumbnail/";
    }

}
// export const Host = 'https://sindibadmarket.com';
export const Host = 'http://gamingo.ir';

